variables:
  SERVICE_NAME: nummerreeks
  SERVICE_DOMAIN: bii
  VAULT_AUTH_ROLE: jwt-nummerreeks
  VAULT_SERVER_URL: https://vault.naturalis.io
  VAULT_PATH: ${SERVICE_DOMAIN}/${SERVICE_NAME}/ansible-nummerreeks

variables:
  MAVEN_IMAGE: maven:3-openjdk-17
  MAVEN_CLI_OPTS: --quiet --batch-mode --settings make/.m2/settings.xml
  IMAGE_BASE_NAME: nummerreeks

stages:
  - check
  - install
  - build
  - test
  - publish
  - generate-scan-jobs
  - scan

include:
  - template: Security/Secret-Detection.gitlab-ci.yml

secret_detection:
  extends: .secret-analyzer
  stage: check
  variables:
    SECRET_DETECTION_HISTORIC_SCAN: "false"
    SECRET_DETECTION_REPORT_FILE: "gl-secret-detection-report.json"
  artifacts:
    reports:
      secret_detection: $SECRET_DETECTION_REPORT_FILE
    expire_in: 7 days
  allow_failure: false
  before_script:
    - apk add --no-cache jq
  script:
    - /analyzer run
    - |
      if [ -f "$SECRET_DETECTION_REPORT_FILE" ]; then
        if [ "$(jq ".vulnerabilities | length" $SECRET_DETECTION_REPORT_FILE)" -gt 0 ]; then
          echo "Vulnerabilities detected!"
          echo "Please check the $SECRET_DETECTION_REPORT_FILE security file."
          echo "Or run gitleak locally on your repository."
          exit 1
        fi
      else
        echo "Artifact $SECRET_DETECTION_REPORT_FILE does not exist."
        echo "No evaluation can be performed."
      fi

lint dockerfile:
  stage: check
  except:
    - schedules
  image: hadolint/hadolint:latest-debian
  tags:
    - stackio
  before_script:
    - mkdir -p reports
  script:
    - hadolint --ignore DL3008 --ignore DL3059 --format gitlab_codeclimate make/Dockerfile > reports/hadolint-$CI_COMMIT_SHA.json
  artifacts:
    name: "hadolint-report"
    expire_in: 1 day
    when: always
    reports:
      codequality:
        - reports/hadolint-$CI_COMMIT_SHA.json
    paths:
      - reports/hadolint-$CI_COMMIT_SHA.json


check code style:
  stage: check
  except:
    - schedules
  image: ${MAVEN_IMAGE}
  tags:
    - stackio
  script:
    - mvn checkstyle:check -Dcheckstyle.config.location=google_checks.xml
  artifacts:
    paths:
      - target/checkstyle-*

maven test:
  stage: check
  except:
    - schedules
  image: ${MAVEN_IMAGE}
  tags:
    - stackio
  script:
    - mvn ${MAVEN_CLI_OPTS} test
  artifacts:
    expire_in: 3 days
    reports:
      junit:
        - target/surefire-reports/TEST-*.xml


maven install:
  stage: install
  except:
    - schedules
  image: ${MAVEN_IMAGE}
  tags:
    - stackio
  before_script:
    - echo ${MAVEN_ARTIFACT}
  script:
    - mvn ${MAVEN_CLI_OPTS} -DskipTests -Dcheckstyle.skip clean install
  artifacts:
    expire_in: 3 days
    paths:
      - make/dependencies


build docker image:
  stage: build
  except:
    - schedules
  image: docker:stable
  tags:
    - stackio
  services:
    - docker:dind
  before_script:
    - echo -n $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
  script:
    - image=${CI_REGISTRY_IMAGE}:build-${CI_PIPELINE_ID}
    - echo Building ${image}
    - ( cd make ; docker build --pull -t "${image}" . )
    - docker push "${image}"


vulnerability scan:
  stage: test
  except:
    - schedules
  image: docker:20
  tags:
    - stackio
  services:
    - docker:20-dind
  cache:
    paths:
      - .trivy/
  before_script:
    - echo -n $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - docker create --name trivy aquasec/trivy:latest
    - docker cp trivy:/usr/local/bin/trivy /usr/local/bin/trivy
    - docker rm -f trivy
    - 'wget --header "PRIVATE-TOKEN: $TRIVYIGNORE_ACCESS_TOKEN" "https://gitlab.com/api/v4/projects/41791412/repository/files/trivyignore/raw?ref=main" --output-document=.trivyignore'
  script:
    - trivy --cache-dir .trivy/ image --removed-pkgs ${CI_REGISTRY_IMAGE}:build-${CI_PIPELINE_ID}


publish docker image:
  stage: publish
  except:
    - schedules
  image: docker:stable
  tags:
    - stackio
  services:
    - docker:dind
  before_script:
    - echo -n $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
  script:
    - ./publish.sh


generate scan jobs:
  only:
    - schedules
  stage: generate-scan-jobs
  image: alpine:latest
  before_script:
    - apk add -U jsonnet curl jq
  script:
    - TAGS=$(echo $( curl "https://gitlab.com/api/v4/projects/23469878/registry/repositories/2986770/tags" | jq 'map(select(.name | match("^latest$|^[0-9]+[.]?[0-9]*"))) | [.[].name]' ))
    - sed -i -r "s/[{]{3}TAGS[}]{3}/$TAGS/" job-blueprint.jsonnet
    - jsonnet job-blueprint.jsonnet > jobs.yml
  artifacts:
    paths:
      - jobs.yml


scan images:
  only:
    - schedules
  stage: scan
  needs:
    - "generate scan jobs"
  trigger:
    include:
    - artifact: jobs.yml
      job: "generate scan jobs"
    strategy: depend 
