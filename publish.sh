#!/bin/sh
set -eu

image=${CI_REGISTRY_IMAGE}:build-${CI_PIPELINE_ID}

docker pull "$image"

if [ -n "${CI_COMMIT_TAG+x}" ]; then
  docker tag "$image" "$CI_REGISTRY_IMAGE:${CI_COMMIT_TAG}"
  docker push "$CI_REGISTRY_IMAGE:${CI_COMMIT_TAG}"
  echo "Published new docker image with label: nummerreeks:${CI_COMMIT_TAG}"
elif [ "${CI_COMMIT_BRANCH}" == "main" ]; then
  docker tag "$image" "$CI_REGISTRY_IMAGE:latest"
  docker push "$CI_REGISTRY_IMAGE:latest"
  echo "Published new docker image with label: nummerreeks:latest"
else
  docker tag "$image" "${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}-${CI_COMMIT_SHORT_SHA}"
  docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}-${CI_COMMIT_SHORT_SHA}"
  echo "Published new docker image with label: nummerreeks:${CI_COMMIT_BRANCH}-${CI_COMMIT_SHORT_SHA}"
fi
