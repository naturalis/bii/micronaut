package nl.naturalis.bcd;

import javax.ws.rs.core.UriInfo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import nl.naturalis.bcd.migrate.MigrateDbCommand;
import nl.naturalis.common.check.Check;
import org.pac4j.dropwizard.Pac4jFactory;

import static nl.naturalis.common.check.CommonChecks.illegalState;
import static nl.naturalis.common.check.CommonChecks.notNull;

public class BcdConfig extends Configuration {

  // Set in BcdServer.run()
  static BcdConfig INSTANCE;

  @JsonProperty("pac4j")
  Pac4jFactory pac4jFactory = new Pac4jFactory();

  public static BcdConfig getInstance() {
    Check.on(illegalState(), INSTANCE).is(notNull(), "Configuration not available yet");
    return INSTANCE;
  }

  /**
   * The base URL under which the Nummerapplicatie server runs. Ordinarily the service can figure this out
   * by itself (by calling {@link UriInfo#getBaseUri() UriInfo.getBaseUri}). However, if the service
   * runs behind a load balancer or firewall, it could be that the server sees a different base URL
   * than the user. You must then explicitly set the base URL to what the user sees in his/her
   * browser.
   */
  public String baseUrl;

  /** Database connection settings */
  public DbConfig database;

  /** Database connection settings for legacy database. Used for the {@link MigrateDbCommand}. */
  public DbConfig legacy;
}
