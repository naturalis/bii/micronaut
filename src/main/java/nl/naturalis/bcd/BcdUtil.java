package nl.naturalis.bcd;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import nl.naturalis.bcd.exception.BcdUserInputException;
import nl.naturalis.common.check.Check;

public class BcdUtil {

  public static String ERR_INVALID_TOPDESK_ID =
      "Ongeldig TOPDesk nummer: \"%s\". Oude TOPDesk nummers moeten beginnen met "
          + "\"M\" of \"Onbekend\" en eindigen met 1 of meer cijfers. Nieuwe "
          + "TOPDesk nummers moeten beginnen met \"M\" en eindigen met 1 of meer "
          + "cijfers.";

  private static final String REGEX_TOPDESK_ID = "^(M\\d+)( \\(\\d+\\))?$";
  private static final String REGEX_LEGACY_TOPDESK_ID = "^((M|Onbekend)\\d+)( \\(\\d+\\))?$";

  private static final Pattern TOPDESK_ID_PATTERN = Pattern.compile(REGEX_TOPDESK_ID);
  private static final Pattern LEGACY_TOPDESK_ID_PATTERN = Pattern.compile(
      REGEX_LEGACY_TOPDESK_ID);

  private BcdUtil() {
  }

  public static <T> Check<T, BcdUserInputException> checkInput(T value) {
    return Check.on(BcdUserInputException::new, value);
  }

  public static Check<Integer, BcdUserInputException> checkInput(int value) {
    return Check.on(BcdUserInputException::new, value);
  }

  public static boolean isValidTopDeskId(String topDeskId) {
    return TOPDESK_ID_PATTERN.matcher(topDeskId).matches();
  }

  public static boolean isValidLegacyTopDeskId(String topDeskId) {
    return LEGACY_TOPDESK_ID_PATTERN.matcher(topDeskId).matches();
  }

  public static String extractTopDeskId(String legacyTopDeskId) {
    Matcher m = TOPDESK_ID_PATTERN.matcher(legacyTopDeskId);
    if (m.find()) {
      return m.group(1);
    }
    return "INVALID:" + legacyTopDeskId;
  }

  /**
   * <p>addPathToUrl() is a simple method for adding a path to an URL. The method
   * makes sure a forward slash is added or removed when needed, but it does NOT
   * test if the final url is valid or not.</p>
   *
   * @param url provide a valid (!) url
   * @param path provide a non-empty path
   * @return the new url with the path added
   */
  public static String addPathToUrl(String url, String path) {
    if (url == null || url.isEmpty() || path == null || path.isEmpty()) {
      throw new IllegalArgumentException();
    }
    if (url.endsWith("/")) {
      url = url.substring(0, url.length() - 1);
    }
    if (path.endsWith("/")) {
      path = path.substring(0, path.length() - 1);
    }
    if (path.startsWith("/")) {
      path = path.substring(1);
    }
    return url.concat("/").concat(path);
  }
}
