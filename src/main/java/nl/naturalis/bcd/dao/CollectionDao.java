package nl.naturalis.bcd.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;
import org.klojang.db.SQL;
import org.klojang.db.SQLQuery;
import org.klojang.db.SQLUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.managed.Database;
import nl.naturalis.bcd.model.BcdCollection;
import nl.naturalis.common.ExceptionMethods;
import nl.naturalis.common.check.Check;
import static nl.naturalis.bcd.dao.DaoUtil.logSQL;
import static nl.naturalis.common.check.CommonChecks.empty;

public class CollectionDao extends CRUDDao<BcdCollection> {

  private static final Logger LOG = LoggerFactory.getLogger(CollectionDao.class);

  public boolean hasRequests(int value) {
    return super.hasRequests("collectionId", value);
  }

  public int countRequests(int collectionId) {
    SQL sql = getSQL("countRequests", getQueries());
    try (Connection con = Database.pool().getConnection()) {
      try (SQLQuery query = sql.prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        return query.bind("id", collectionId).getInt();
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  public int deactivate(int id) {
    SQL sql = getSQL("deactivate", getQueries());
    try (Connection con = Database.pool().getConnection()) {
      try (SQLUpdate query = sql.prepareUpdate(con)) {
        logSQL(LOG, query.getSQL());
        return query.bind("id", id).execute();
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  public int deleteByInstituteId(int id) {
    SQL sql = getSQL("deleteByInstituteId", getQueries());
    try (Connection con = Database.pool().getConnection()) {
      try (SQLUpdate query = sql.prepareUpdate(con)) {
        logSQL(LOG, query.getSQL());
        return query.bind("id", id).execute();
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  // Used by data migration code
  public BcdCollection find(Connection con, int id) {
    Optional<BcdCollection> opt = super.find(con, BcdCollection.class, BcdCollection::new, id);
    return Check.that(opt).isNot(empty(), "No such collection ID: %d", id).ok(Optional::get);
  }

  @Override
  Queries getQueries() {
    return Queries.collection();
  }
}
