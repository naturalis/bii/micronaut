package nl.naturalis.bcd.dao;

import org.klojang.db.SQL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class DaoUtil {

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(DaoUtil.class);

  static void logSQL(Logger logger, SQL sql) {
    logSQL(logger, sql.toString());
  }

  static void logSQL(Logger logger, String sql) {
    if (logger.isDebugEnabled()) {
      logger.debug(sql.replaceAll("\\s+", " "));
    }
  }
}
