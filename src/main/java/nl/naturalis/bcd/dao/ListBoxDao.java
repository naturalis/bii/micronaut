package nl.naturalis.bcd.dao;

import java.sql.Connection;
import java.util.EnumMap;
import java.util.List;
import org.klojang.db.Row;
import org.klojang.db.SQL;
import org.klojang.db.SQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.listbox.ListBoxType;
import nl.naturalis.bcd.managed.Database;
import nl.naturalis.bcd.model.LayoutInputType;
import nl.naturalis.common.ExceptionMethods;

import static nl.naturalis.bcd.dao.DaoUtil.logSQL;
import static nl.naturalis.bcd.dao.Queries.listbox;
import static nl.naturalis.bcd.listbox.ListBoxType.*;
import static nl.naturalis.bcd.model.Institute.CORRUPT_CODE;
import static nl.naturalis.bcd.model.Institute.OVERLAP_CODE;
import static nl.naturalis.common.ObjectMethods.ifNotNull;
import static nl.naturalis.common.ObjectMethods.ifNull;

public class ListBoxDao {

  private static final Logger LOG = LoggerFactory.getLogger(ListBoxDao.class);

  private static final EnumMap<ListBoxType, SQL> queries = new EnumMap<>(ListBoxType.class);

  public ListBoxDao() {
  }

  public List<Row> cmsOptions(int id) {
    SQL sql = queries.computeIfAbsent(CMS, k -> SQL.create(listbox().cmsOptions()));
    try (Connection con = Database.pool().getConnection()) {
      try (SQLQuery query = sql.prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        return query.bind("id", id).getMappifier().mappifyAll();
      }
    } catch (Throwable t) {
      throw ExceptionMethods.uncheck(t);
    }
  }

  public List<Row> departmentOptions(int id) {
    SQL sql = queries.computeIfAbsent(DEPARTMENT, k -> SQL.create(listbox().departmentOptions()));
    try (Connection con = Database.pool().getConnection()) {
      try (SQLQuery query = sql.prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        return query.bind("id", id).getMappifier().mappifyAll();
      }
    } catch (Throwable t) {
      throw ExceptionMethods.uncheck(t);
    }
  }

  public List<Row> instituteOptions(int id) {
    return instituteOptions(id, true);
  }

  public List<Row> instituteOptions(int id, boolean excludeDummies) {
    SQL sql = queries.computeIfAbsent(INSTITUTE, k -> SQL.create(listbox().instituteOptions()));
    String overlap = excludeDummies ? OVERLAP_CODE : "";
    String corrupt = excludeDummies ? CORRUPT_CODE : "";
    try (Connection con = Database.pool().getConnection()) {
      try (SQLQuery query = sql.prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        return query.bind("id", id)
            .bind("overlap", overlap)
            .bind("corrupt", corrupt)
            .getMappifier()
            .mappifyAll();
      }
    } catch (Throwable t) {
      throw ExceptionMethods.uncheck(t);
    }
  }

  public List<Row> collectionOptions(int instituteId, Integer collectionId) {
    SQL sql = queries.computeIfAbsent(COLLECTION, k -> SQL.create(listbox().collectionOptions()));
    try (Connection con = Database.pool().getConnection()) {
      try (SQLQuery query = sql.prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        return query.bind("instituteId", instituteId)
            .bind("collectionId", ifNull(collectionId, -1))
            .getMappifier()
            .mappifyAll();
      }
    } catch (Throwable t) {
      throw ExceptionMethods.uncheck(t);
    }
  }

  public List<Row> requesterOptions(int id) {
    SQL sql = queries.computeIfAbsent(REQUESTER, k -> SQL.create(listbox().requesterOptions()));
    try (Connection con = Database.pool().getConnection()) {
      try (SQLQuery query = sql.prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        return query.bind("id", id).getMappifier().mappifyAll();
      }
    } catch (Throwable t) {
      throw ExceptionMethods.uncheck(t);
    }
  }

  public List<Row> layoutOptions(String inputType) {
    LayoutInputType layoutInputType = ifNotNull(inputType, LayoutInputType::parse);
    SQL sql = queries.computeIfAbsent(LAYOUT, k -> SQL.create(listbox().layoutOptions()));
    try (Connection con = Database.pool().getConnection()) {
      try (SQLQuery query = sql.prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        return query.bind("inputType", layoutInputType).getMappifier().mappifyAll();
      }
    } catch (Throwable t) {
      throw ExceptionMethods.uncheck(t);
    }
  }
}
