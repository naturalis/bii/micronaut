package nl.naturalis.bcd.dao;

import java.io.InputStream;
import nl.naturalis.bcd.exception.BcdRuntimeException;
import nl.naturalis.common.ExceptionMethods;
import nl.naturalis.common.IOMethods;
import nl.naturalis.common.StringMethods;

/**
 * Provides access to the file contents of the SQL files.
 */
abstract class Queries {

  static SeriesQueries series() {
    return new SeriesQueries();
  }

  static RequesterQueries requester() {
    return new RequesterQueries();
  }

  static UserQueries user() {
    return new UserQueries();
  }

  static InstituteQueries institute() {
    return new InstituteQueries();
  }

  static CollectionQueries collection() {
    return new CollectionQueries();
  }

  static DepartmentQueries department() {
    return new DepartmentQueries();
  }

  static CmsQueries cms() {
    return new CmsQueries();
  }

  static ListBoxQueries listbox() {
    return new ListBoxQueries();
  }

  static LayoutQueries layout() {
    return new LayoutQueries();
  }

  static StorageLocationQueries storageLocation() {
    return new StorageLocationQueries();
  }

  static GenericQueries generic() {
    return new GenericQueries();
  }

  /* ============================================================= */
  /* Number Series queries                                         */
  /* ============================================================= */

  static class SeriesQueries extends Queries {}

  /* ============================================================= */
  /* Requester queries                                             */
  /* ============================================================= */

  static class RequesterQueries extends Queries {}

  /* ============================================================= */
  /* User queries                                                  */
  /* ============================================================= */

  static class UserQueries extends Queries {}

  /* ============================================================= */
  /* Institute queries                                             */
  /* ============================================================= */

  static class InstituteQueries extends Queries {}

  /* ============================================================= */
  /* Institute queries                                             */
  /* ============================================================= */

  static class CollectionQueries extends Queries {}

  /* ============================================================= */
  /* Department queries                                            */
  /* ============================================================= */

  static class DepartmentQueries extends Queries {}

  /* ============================================================= */
  /* CMS queries                                                   */
  /* ============================================================= */

  static class CmsQueries extends Queries {}

  /* ============================================================= */
  /* List box population queries                                    */
  /* ============================================================= */

  static class ListBoxQueries extends Queries {

    String cmsOptions() {
      return loadQuery("cmsOptions");
    }

    String departmentOptions() {
      return loadQuery("departmentOptions");
    }

    String instituteOptions() {
      return loadQuery("instituteOptions");
    }

    String collectionOptions() {
      return loadQuery("collectionOptions");
    }

    String requesterOptions() {
      return loadQuery("requesterOptions");
    }

    String layoutOptions() {
      return loadQuery("layoutOptions");
    }
  }

  /* ============================================================= */
  /* Layout configuration queries                                  */
  /* ============================================================= */

  static class LayoutQueries extends Queries {}

  /* ============================================================= */
  /* Storage Location queries                                      */
  /* ============================================================= */

  static class StorageLocationQueries extends Queries {}

  /* ============================================================= */
  /* Generic queries                                               */
  /* ============================================================= */

  static class GenericQueries extends Queries {}

  String loadQuery(String basename) {
    return loadQuery(this, basename);
  }

  String getPath(String basename) {
    return getPath(this, basename);
  }

  static String loadQuery(Queries queries, String basename) {
    String path = getPath(queries, basename);
    try (InputStream in = queries.getClass().getResourceAsStream(path)) {
      if (in == null) {
        String fmt = "No such query in %s: \"%s\"";
        String cname = queries.getClass().getSimpleName();
        throw new BcdRuntimeException(String.format(fmt, cname, basename));
      }
      return IOMethods.toString(in);
    } catch (Exception e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  static String getPath(Queries queries, String basename) {
    // Chop off the "Queries" part of the Queries subclass, so
    // you're left with the name of the entity (or model object).
    String entity = StringMethods.rchop(queries.getClass().getSimpleName(), "Queries");
    return "/sql/" + entity + "." + basename + ".sql";
  }
}
