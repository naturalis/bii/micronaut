package nl.naturalis.bcd.dao;

import static nl.naturalis.bcd.dao.CRUDDao.BIND_INFO;
import static nl.naturalis.bcd.dao.CRUDDao.getDynamicSQL;
import static nl.naturalis.bcd.dao.DaoUtil.logSQL;
import static nl.naturalis.bcd.search.Entity.CMS;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import nl.naturalis.bcd.managed.Database;
import nl.naturalis.bcd.search.Entity;
import nl.naturalis.bcd.search.LabelSearch;
import nl.naturalis.bcd.search.SeriesSearchRequest;
import nl.naturalis.common.CollectionMethods;
import nl.naturalis.common.ExceptionMethods;
import org.klojang.db.Row;
import org.klojang.db.SQL;
import org.klojang.db.SQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helps to build queries for the list page. NB these are the only queries that we build
 * dynamically (i.e. programmatically). Not only that, but the SQL template variables (~%something%)
 * again contain SQL named parameters (:something). So which parameters to bind really depends on
 * how you build the query.
 */
class SeriesDaoHelper {

  private static final Logger LOG = LoggerFactory.getLogger(SeriesDaoHelper.class);

  static final Class<? extends CRUDDao> CLAZZ = SeriesDao.class;

  /**
   * Maps the entities in the domain model to the primary key of the corresponding tables.
   */
  private static final EnumMap<Entity, String> PRIMARY_KEY_COLS =
      CollectionMethods.saturatedEnumMap(Entity.class,
          "a.cmsId",
          "a.collectionId",
          "a.departmentId",
          "b.instituteId",
          "SERIES_ENTITY_NOT_ALLOWED",
          "a.requesterId",
          "STORAGE_ENTITY_NOT_ALLOWED",
          "LAYOUT_ENTITY_NOT_ALLOWED",
          "a.modifiedBy");

  /**
   * Counts rows for search requests without a search term, or with a search term that does not
   * contain a dot, or with a search that does contain a dot, but is enclosed in single/double
   * quotes. A.k.a. Tekst Search.
   */
  static int countRowsNormalSearch(SeriesSearchRequest ssr, int sessionUserId) {
    try (Connection con = Database.pool().getConnection()) {
      SQL sql = getDynamicSQL(CLAZZ, "count", Queries.series(), BIND_INFO);
      sql.set("cmsJoin", getCmsJoin(ssr));
      sql.set("whereClause", getWhereClauseNormalSearch(ssr));
      try (SQLQuery query = sql.prepareQuery(con)) {
        DaoUtil.logSQL(LOG, query.getSQL());
        query.bind(ssr);
        if (ssr.isMineOnly()) {
          query.bind("sessionUserId", sessionUserId);
        }
        return query.getInt();
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  /**
   * Retrieves rows for search requests without a search term, or with a search term that does not
   * contain a dot, or with a search that does contain a dot, but is enclosed in single/double
   * quotes. A.k.a. Tekst Search.
   */
  static List<Row> normalSearch(SeriesSearchRequest ssr, int sessionUserId) {
    try (Connection con = Database.connect()) {
      SQL sql = getDynamicSQL(CLAZZ, "list", Queries.series(), BIND_INFO);
      sql.set("cmsJoin", getCmsJoin(ssr));
      sql.set("whereClause", getWhereClauseNormalSearch(ssr));
      sql.setOrderBy(ssr.getSortColumnLabel(), ssr.getSortOrder());
      try (SQLQuery query = sql.prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        query.bind(ssr);
        if (ssr.isMineOnly()) {
          query.bind("sessionUserId", sessionUserId);
        }
        return query.getMappifier().mappifyAll(ssr.getLimit());
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  /**
   * Counts rows for search requests containing a search term with a dot somewhere, which will
   * trigger a label search a.k.a. Registratienummer Search.
   */
  static int countRowsLabelSearch(SeriesSearchRequest ssr, LabelSearch labelSearch,
      int sessionUserId) {
    try (Connection con = Database.pool().getConnection()) {
      SQL sql = getDynamicSQL(CLAZZ, "countUsingLabel", Queries.series(), BIND_INFO);
      sql.set("whereClause", getWhereClauseLabelSearch(ssr, labelSearch));
      try (SQLQuery query = sql.prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        query.bind(ssr).bind(labelSearch);
        if (ssr.isMineOnly()) {
          query.bind("sessionUserId", sessionUserId);
        }
        return query.getInt();
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  /**
   * Retrieves rows for search requests containing a search term with a dot somewhere, which will
   * trigger a label search a.k.a. Registratienummer Search.
   */
  static List<Row> labelSearch(SeriesSearchRequest ssr, LabelSearch labelSearch,
      int sessionUserId) {
    try (Connection con = Database.pool().getConnection()) {
      SQL sql = getDynamicSQL(CLAZZ, "listUsingLabel", Queries.series(), BIND_INFO);
      sql.set("whereClause", getWhereClauseLabelSearch(ssr, labelSearch));
      sql.setOrderBy(ssr.getSortColumnLabel(), ssr.getSortOrder());
      try (SQLQuery query = sql.prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        query.bind(ssr).bind(labelSearch);
        if (ssr.isMineOnly()) {
          query.bind("sessionUserId", sessionUserId);
        }
        return query.getMappifier().mappifyAll(ssr.getLimit());
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  private static String getCmsJoin(SeriesSearchRequest ssr) {
    if (CMS.equals(ssr.getEntity()) || ssr.getNormalizedSearch() != null) {
      return "JOIN bcd_cms f ON a.cmsId = f.cmsId";
    }
    return null;
  }

  private static String getWhereClauseNormalSearch(SeriesSearchRequest ssr) {
    List<String> clauses = new ArrayList<>(5);
    if (ssr.getEntity() != null) {
      clauses.add(PRIMARY_KEY_COLS.get(ssr.getEntity()) + " = :entityId");
    }
    if (ssr.isPendingOnly()) {
      clauses.add("a.status IN (0,1)");
    }
    if (ssr.isMineOnly()) {
      clauses.add("a.modifiedBy = :sessionUserId");
    }
    if (ssr.getNormalizedSearch() != null) {
      clauses.add("(INSTR(UPPER(a.topDeskId), :normalizedSearch) != 0"
          + " OR INSTR(UPPER(d.requesterName), :normalizedSearch) != 0"
          + " OR INSTR(UPPER(e.departmentName), :normalizedSearch) != 0"
          + " OR INSTR(UPPER(f.cmsName), :normalizedSearch) != 0"
          + " OR INSTR(UPPER(a.comments), :normalizedSearch) != 0)");
    }
    if (clauses.size() == 0) {
      return null;
    }
    return "WHERE " + CollectionMethods.implode(clauses, " AND ");
  }

  private static String getWhereClauseLabelSearch(SeriesSearchRequest ssr,
      LabelSearch labelSearch) {
    List<String> clauses = new ArrayList<>(5);
    if (ssr.getEntity() != null) {
      clauses.add(PRIMARY_KEY_COLS.get(ssr.getEntity()) + " = :entityId");
    }
    if (ssr.isPendingOnly()) {
      clauses.add("a.status IN (0,1)");
    }
    if (ssr.isMineOnly()) {
      clauses.add("a.modifiedBy = :sessionUserId");
    }
    if (ssr.getNormalizedSearch() != null) {
      if (labelSearch.getNumber() != -1) {
        clauses.add(":number BETWEEN a.firstNumber AND a.lastNumber");
      }
      if (labelSearch.getText() != null) {
        StringBuilder sb = new StringBuilder(128);
        sb.append('(');
        sb.append("INSTR(UPPER(b.collectionCode), :text) != 0");
        if (labelSearch.getCollection() == null) {
          sb.append(" OR INSTR(UPPER(b.collectionCode), :institute) != 0");
          sb.append(" OR INSTR(UPPER(c.instituteCode), :institute) != 0");
        } else {
          sb.append(" OR (INSTR(UPPER(b.collectionCode), :collection) != 0");
          sb.append(" AND INSTR(UPPER(c.instituteCode), :institute) != 0)");
        }
        sb.append(')');
        clauses.add(sb.toString());
      }
    }
    if (clauses.size() == 0) {
      return null; //
    }
    return "WHERE " + CollectionMethods.implode(clauses, " AND ");
  }

}
