package nl.naturalis.bcd.dao;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;
import java.util.function.Supplier;
import org.klojang.db.SQL;
import org.klojang.db.SQLInsert;
import org.klojang.db.SQLQuery;
import org.klojang.db.SQLUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.exception.BcdUserInputException;
import nl.naturalis.bcd.managed.Database;
import nl.naturalis.bcd.model.StorageLabel;
import nl.naturalis.bcd.model.StorageLocation;
import nl.naturalis.common.check.Check;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toCollection;
import static nl.naturalis.bcd.dao.DaoUtil.logSQL;
import static nl.naturalis.common.ObjectMethods.isNotEmpty;
import static nl.naturalis.common.check.CommonChecks.NULL;
import static nl.naturalis.common.check.CommonChecks.empty;
import static nl.naturalis.common.check.CommonChecks.notNull;
import static nl.naturalis.common.check.CommonChecks.unsupportedOperation;

public class StorageLocationDao extends CRUDDao<StorageLocation> {

  private static final Logger LOG = LoggerFactory.getLogger(StorageLocationDao.class);

  public Optional<StorageLocation> doFind(int id) throws SQLException {
    Optional<StorageLocation> opt = super.find(StorageLocation.class, StorageLocation::new, id);
    if (!opt.isEmpty()) {
      StorageLocation sl = opt.get();
      sl.setLabels(loadLabels(sl));
    }
    return opt;
  }

  /** Returns the StorageLocation but without the associated StorageLabel records. */
  public Optional<StorageLocation> findPlain(int id) {
    return super.find(StorageLocation.class, StorageLocation::new, id);
  }

  /** Updates the specified StorageLocation, but performs no label-related logic. */
  public void updatePlain(StorageLocation storageLocation) {
    super.update(storageLocation);
  }

  public StorageLocation doInsert(StorageLocation storageLocation)
      throws IOException, SQLException {
    createLabels(storageLocation);
    extractPrefixes(storageLocation);
    try (Connection con = Database.pool().getConnection()) {
      con.setAutoCommit(false);
      try {
        txInsert(con, storageLocation);
        setForeignKeyOnLabels(storageLocation);
        txInsertLabels(con, storageLocation);
        con.commit();
      } catch (Throwable t) {
        con.rollback();
        throw t;
      } finally {
        con.setAutoCommit(true);
      }
    }
    return storageLocation;
  }

  public StorageLocation doUpdate(StorageLocation storageLocation, boolean newPrefixSegmentCount)
      throws SQLException, IOException {
    if (isNotEmpty(storageLocation.getUpload())) {
      replaceLabelsAndUpdate(storageLocation);
    } else if (newPrefixSegmentCount) {
      regenerateLabelsAndUpdate(storageLocation);
    } else {
      super.update(storageLocation);
    }
    return storageLocation;
  }

  public int doDelete(Integer storageLocationId) throws SQLException {
    StorageLocation sl = new StorageLocation();
    sl.setStorageLocationId(storageLocationId);
    try (Connection con = Database.pool().getConnection()) {
      con.setAutoCommit(false);
      try {
        txDeleteLabels(con, sl);
        int deleted = txDelete(con, sl);
        con.commit();
        return deleted;
      } catch (Throwable t) {
        con.rollback();
        throw t;
      } finally {
        con.setAutoCommit(true);
      }
    }
  }

  private void replaceLabelsAndUpdate(StorageLocation storageLocation)
      throws IOException, SQLException {
    createLabels(storageLocation);
    setForeignKeyOnLabels(storageLocation);
    extractPrefixes(storageLocation);
    executeUpdate(storageLocation);
  }

  private void regenerateLabelsAndUpdate(StorageLocation storageLocation) throws SQLException {
    regenerateLabels(storageLocation);
    setForeignKeyOnLabels(storageLocation);
    extractPrefixes(storageLocation);
    executeUpdate(storageLocation);
  }

  private void executeUpdate(StorageLocation storageLocation) throws SQLException {
    try (Connection con = Database.pool().getConnection()) {
      con.setAutoCommit(false);
      try {
        txDeleteLabels(con, storageLocation);
        txInsertLabels(con, storageLocation);
        txUpdate(con, storageLocation);
        con.commit();
      } catch (Throwable t) {
        con.rollback();
        throw t;
      } finally {
        con.setAutoCommit(true);
      }
    }
  }

  private int txInsert(Connection con, StorageLocation storageLocation) {
    Check.that(storageLocation.getStorageLocationId())
        .is(NULL(), "ID must not be set when inserting storage location");
    Check.that(storageLocation.getUpload()).isNot(empty(), "Missing file upload");
    SQL sql = getSQL("insert", getQueries());
    try (SQLInsert query = sql.prepareInsert(con)) {
      logSQL(LOG, query.getSQL());
      int id = (int) query.bind(storageLocation).executeAndGetId();
      storageLocation.setStorageLocationId(id);
      return id;
    }
  }

  private static void setForeignKeyOnLabels(StorageLocation storageLocation) {
    int id = storageLocation.getStorageLocationId();
    storageLocation.getLabels().forEach(l -> l.setStorageLocationId(id));
  }

  private void txInsertLabels(Connection con, StorageLocation storageLocation) {
    Check.that(storageLocation.getStorageLocationId())
        .is(notNull(), "Cannot insert storage location labels without ID");
    Check.that(storageLocation.getLabels())
        .isNot(empty(), "At least one storage location label required for save action");
    SQL sql = getSQL("insertLabel", getQueries());
    try (SQLInsert query = sql.prepareInsert(con)) {
      logSQL(LOG, query.getSQL());
      query.insertAll(storageLocation.getLabels());
    }
  }

  private void txUpdate(Connection con, StorageLocation storageLocation) {
    Check.that(storageLocation.getStorageLocationId())
        .is(notNull(), "Cannot update storage location without ID");
    SQL sql = getSQL("update", getQueries());
    try (SQLUpdate query = sql.prepareUpdate(con)) {
      logSQL(LOG, query.getSQL());
      query.bind(storageLocation).execute();
    }
  }

  private int txDelete(Connection con, StorageLocation storageLocation) {
    Integer id = storageLocation.getStorageLocationId();
    Check.that(id).is(notNull(), "Cannot delete storage location without ID");
    SQL sql = getSQL("delete", getQueries());
    try (SQLUpdate query = sql.prepareUpdate(con)) {
      logSQL(LOG, query.getSQL());
      return query.bind("id", id).execute();
    }
  }

  private void txDeleteLabels(Connection con, StorageLocation storageLocation) {
    Integer id = storageLocation.getStorageLocationId();
    Check.that(id).is(notNull(), "Cannot delete storage location labels without ID");
    SQL sql = getSQL("deleteLabels", getQueries());
    try (SQLUpdate query = sql.prepareUpdate(con)) {
      logSQL(LOG, query.getSQL());
      query.bind("id", id).execute();
    }
  }

  private List<StorageLabel> loadLabels(StorageLocation storageLocation) throws SQLException {
    SQL sql = getSQL("getLabels", getQueries());
    try (Connection con = Database.pool().getConnection()) {
      try (SQLQuery query = sql.prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        return query
            .bind(storageLocation)
            .getBeanifier(StorageLabel.class, StorageLabel::new)
            .beanifyAll();
      }
    }
  }

  private static void createLabels(StorageLocation storageLocation) throws IOException {
    Check.that(storageLocation.getPrefixSegmentCount())
        .is(notNull(), "Cannot create labels without a prefix segment count");
    Reader r = new InputStreamReader(new ByteArrayInputStream(storageLocation.getUpload()), UTF_8);
    LineNumberReader lnr = new LineNumberReader(r);
    if (lnr.readLine() == null) { // Skip header
      throw new BcdUserInputException("Leeg bestand");
    }
    storageLocation.setLabels(new ArrayList<>(50));
    for (String s = lnr.readLine(); s != null; s = lnr.readLine()) {
      if (!s.isBlank()) {
        createLabel(storageLocation, s, lnr.getLineNumber());
      }
    }
    if (storageLocation.getLabels().isEmpty()) {
      throw new BcdUserInputException("Leeg bestand");
    }
  }

  private static void regenerateLabels(StorageLocation storageLocation) {
    Check.that(storageLocation.getLabels())
        .isNot(empty(), "Cannot regenerate labels when not loaded first");
    Check.that(storageLocation.getPrefixSegmentCount())
        .is(notNull(), "Cannot regenerate labels without a prefix segment count");
    List<StorageLabel> oldLabels = storageLocation.getLabels();
    List<StorageLabel> newLabels = new ArrayList<>(oldLabels.size());
    int prefixSegmentCount = storageLocation.getPrefixSegmentCount();
    for (int i = 0; i < oldLabels.size(); ++i) {
      String label = oldLabels.get(i).toString();
      newLabels.add(new StorageLabel(label, prefixSegmentCount, i));
    }
    storageLocation.setLabels(newLabels);
  }

  private static void createLabel(StorageLocation storageLocation, String line, int lineNo) {
    int i = line.indexOf(';');
    if (i == -1) {
      String err = String.format("Regel %d: veld-scheidingteken (';') ontbreekt", lineNo + 1);
      throw new BcdUserInputException(err);
    }
    String label = line.substring(0, i).strip();
    if (label.isBlank() || label.endsWith(".") || label.startsWith(".")) {
      throw new BcdUserInputException(
          String.format("Regel %d: ongeldig label: \"%s\"", lineNo + 1, label));
    }
    int prefixSegmentCount = storageLocation.getPrefixSegmentCount();
    storageLocation.getLabels().add(new StorageLabel(label, prefixSegmentCount, lineNo));
  }

  /*
   * Extracts the prefix segments from the labels and puts them back onto the StorageLocation
   * instance. Since the "prefixes" property of StorageLocation is a persistent property, this
   * method must be called __before__ inserting or updating the StorageLocation itself.
   */
  private static void extractPrefixes(StorageLocation storageLocation) {
    storageLocation.setPrefixes(
        storageLocation
            .getLabels()
            .stream()
            .map(StorageLabel::getCoarseGrainedLocation)
            .collect(toCollection(TreeSet::new))
            .stream()
            .collect(joining(", ")));
  }

  @Override
  Queries getQueries() {
    return Queries.storageLocation();
  }

  @Override
  public Optional<StorageLocation> find(
      Class<StorageLocation> modelClass, Supplier<StorageLocation> instanceSupplier, int id) {
    return Check.fail(unsupportedOperation());
  }

  @Override
  public int insert(StorageLocation model) {
    return Check.fail(unsupportedOperation());
  }

  @Override
  public void update(StorageLocation model) {
    Check.fail(unsupportedOperation());
  }

  @Override
  public int delete(int id) {
    return Check.fail(unsupportedOperation());
  }
}
