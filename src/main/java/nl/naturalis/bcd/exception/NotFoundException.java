package nl.naturalis.bcd.exception;

import nl.naturalis.bcd.search.Entity;

import static nl.naturalis.common.StringMethods.concat;

// NB We should use this exception and ditch H404Exception
public class NotFoundException extends BcdUserInputException {

  public NotFoundException(Entity entity, Object keyVal) {
    this(entity, "ID", keyVal);
  }

  public NotFoundException(Entity entity, String keyName, Object keyVal) {
    super(createMessage(entity, keyName, keyVal));
  }

  private static String createMessage(Entity entity, String keyName, Object keyVal) {
    String quote = keyVal.getClass() == String.class ? "\"" : "";
    return concat("Er bestaat geen ",
        entity.getDescription().toLowerCase(),
        " met ",
        keyName,
        " ",
        quote,
        keyVal,
        quote);
  }
}
