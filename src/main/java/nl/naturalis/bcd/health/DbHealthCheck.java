package nl.naturalis.bcd.health;

import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.codahale.metrics.health.HealthCheck;
import nl.naturalis.bcd.managed.Database;
import nl.naturalis.common.ExceptionMethods;

public class DbHealthCheck extends HealthCheck {

  private static final Logger LOG = LoggerFactory.getLogger(DbHealthCheck.class);

  @Override
  protected Result check() {
    // If docker-compose is bootstrapping the entire database it might come
    // online too for a single ping to succeed.
    SQLException exc = null;
    for (int i = 0; i < 5; ++i) {
      LOG.info("Testing database connection");
      try {
        Database.ping();
        return Result.healthy();
      } catch (SQLException e) {
        exc = e;
        LOG.warn("Failed to connect to database. Will try again in 3 seconds ...");
        try {
          Thread.sleep(3000);
        } catch (InterruptedException ie) {
          throw ExceptionMethods.uncheck(ie);
        }
      }
    }
    if (exc == null) {
      return Result.unhealthy("Failed to connect to database");
    }
    return Result.unhealthy(ExceptionMethods.getDetailedMessage(exc));
  }
}
