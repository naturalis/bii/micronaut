package nl.naturalis.bcd.jaxrs;

import nl.naturalis.bcd.exception.BcdUserException;

class BcdUserExceptionInfo extends AbstractExceptionInfo<BcdUserException> {

  @Override
  public boolean isUserError() {
    return true;
  }

  @Override
  public Class<BcdUserException> getExceptionClass() {
    return BcdUserException.class;
  }
}
