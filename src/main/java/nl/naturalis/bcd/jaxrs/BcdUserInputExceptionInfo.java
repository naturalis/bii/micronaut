package nl.naturalis.bcd.jaxrs;

import java.util.Optional;
import nl.naturalis.bcd.exception.BcdUserInputException;

class BcdUserInputExceptionInfo extends AbstractExceptionInfo<BcdUserInputException> {

  @Override
  public boolean isUserError() {
    return true;
  }

  @Override
  public Optional<String> getHeader() {
    return Optional.of("Ongeldige invoer");
  }

  @Override
  public Class<BcdUserInputException> getExceptionClass() {
    return BcdUserInputException.class;
  }
}
