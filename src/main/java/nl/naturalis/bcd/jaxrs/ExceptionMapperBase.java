package nl.naturalis.bcd.jaxrs;

import java.util.List;
import java.util.Set;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.klojang.template.RenderSession;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.common.ExceptionMethods;
import nl.naturalis.common.collection.TypeMap;
import nl.naturalis.common.exception.UncheckedException;

import static javax.ws.rs.core.MediaType.TEXT_HTML_TYPE;
import static nl.naturalis.bcd.resource.ResourceUtil.newRenderSession;
import static nl.naturalis.common.ExceptionMethods.getRootStackTrace;

/**
 * We have just one JAX-RS ExceptionMapper - catering all exceptions (Throwable). Based on the
 * actual type of the actual type of the exception, we retrieve an ExceptionInfo object that tells
 * us how scary we should make the error page.
 */
@Provider
public class ExceptionMapperBase implements ExceptionMapper<Throwable> {

  private static final Logger LOG = LoggerFactory.getLogger(ExceptionMapperBase.class);

  static final TypeMap<ExceptionInfo<?>> EXCEPTION_INFO = loadExceptionMapperInfo();

  @Override
  public Response toResponse(Throwable exc) {
    if (exc.getClass() == UncheckedException.class) {
      exc = ((UncheckedException) exc).unwrap();
    }
    ExceptionInfo<?> info = EXCEPTION_INFO.get(exc.getClass());
    LOG.trace("Retrieving ExceptionInfo for {}: {}",
        exc.getClass().getSimpleName(),
        info.getClass().getSimpleName());
    if (info.isUserError()) {
      return showUserError(exc, info);
    }
    return showError(exc, info);
  }

  private static Response showError(Throwable exc, ExceptionInfo<?> info) {
    String stackTrace = ExceptionMethods.getRootStackTraceAsString(exc, "naturalis", "klojang");
    LOG.error(stackTrace);
    try {
      RenderSession session = newRenderSession("Error");
      session.show("navbar", "logo");
      if (info.getHeader().isEmpty()) {
        session.in("serious").set("display", "none");
      } else {
        session.in("serious").set("display", "block").set("header", info.getHeader().get());
      }
      if (info.showStackTrace()) {
        session.in("serious")
            .set("stackTrace", List.of(getRootStackTrace(exc, "naturalis", "klojang")), "<br>");
      }
      session.in("serious").set("message", exc.getMessage());
      StreamingOutput out = session::render;
      return Response.status(info.getStatus()).type(TEXT_HTML_TYPE).entity(out).build();
    } catch (Exception e) {
      throw new WebApplicationException(e);
    }
  }

  private static Response showUserError(Throwable exc, ExceptionInfo<?> info) {
    LOG.warn(exc.getMessage());
    try {
      RenderSession session = newRenderSession("Error");
      session.show("navbar", "logo");
      if (info.getHeader().isEmpty()) {
        session.in("warning").set("display", "none");
      } else {
        session.in("warning").set("display", "block").set("header", info.getHeader().get());
      }
      session.in("warning").set("message", exc.getMessage());
      StreamingOutput out = session::render;
      return Response.status(info.getStatus()).type(TEXT_HTML_TYPE).entity(out).build();
    } catch (Exception e) {
      throw new WebApplicationException(e);
    }
  }

  @SuppressWarnings("rawtypes")
  private static TypeMap<ExceptionInfo<?>> loadExceptionMapperInfo() {
    LOG.debug("Creating ExceptionInfo objects");
    Class<ExceptionInfo> clazz = ExceptionInfo.class;
    Reflections reflections = new Reflections(clazz.getPackageName());
    Set<Class<? extends ExceptionInfo>> infoClasses = reflections.getSubTypesOf(clazz);
    TypeMap.Builder<ExceptionInfo> builder = TypeMap.build(clazz);
    infoClasses.stream().filter(c -> c != AbstractExceptionInfo.class).forEach(c -> {
      LOG.trace("--> {}", c.getSimpleName());
      ExceptionInfo<?> info;
      try {
        info = c.getDeclaredConstructor().newInstance();
      } catch (Exception e) {
        LOG.error("Failed to instantiate {}: {}", c.getSimpleName(), e);
        throw ExceptionMethods.uncheck(e);
      }
      builder.add(info.getExceptionClass(), info);
    });
    return builder.freeze();
  }
}
