package nl.naturalis.bcd.jaxrs;

import java.util.Optional;
import javax.ws.rs.core.Response.Status;
import nl.naturalis.bcd.exception.NotAuthorizedException;

class NotAuthorizedExceptionInfo extends AbstractExceptionInfo<NotAuthorizedException> {

  @Override
  public boolean isUserError() {
    return false;
  }

  @Override
  public Optional<String> getHeader() {
    return Optional.of("Onvoldoende Rechten");
  }

  @Override
  public Status getStatus() {
    return Status.FORBIDDEN;
  }

  @Override
  public boolean showStackTrace() {
    return false;
  }

  @Override
  public Class<NotAuthorizedException> getExceptionClass() {
    return NotAuthorizedException.class;
  }
}
