package nl.naturalis.bcd.json;

import java.io.IOException;
import org.klojang.db.Row;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class RowSerializer extends JsonSerializer<Row> {

  @Override
  public void serialize(Row value, JsonGenerator gen, SerializerProvider serializers)
      throws IOException {
    if (value == null) {
      gen.writeNull();
    } else {
      gen.writeObject(value.toMap());
    }
  }
}
