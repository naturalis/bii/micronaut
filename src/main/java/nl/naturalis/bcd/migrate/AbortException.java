package nl.naturalis.bcd.migrate;

public class AbortException extends RuntimeException {

  public AbortException(String message) {
    super("FATAL ERROR. " + message);
  }

  public AbortException(Throwable cause) {
    super("FATAL ERROR. " + cause.getMessage(), cause);
  }
}
