select CONCAT(
	'private ',
	CASE DATA_TYPE WHEN 'VARCHAR' THEN 'String' WHEN 'date' THEN 'LocalDateTime' ELSE DATA_TYPE END,
	' ',
	column_name,
	';')
FROM information_schema.COLUMNS
WHERE TABLE_NAME = 'nbc_prefix'