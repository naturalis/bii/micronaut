package nl.naturalis.bcd.migrate;

/**
 * Transforms a V1 model object into a V2 model object
 *
 * @param <T>
 */
public interface SelfTransformer<T> {

  T morph() throws MigrationException;
}
