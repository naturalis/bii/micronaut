package nl.naturalis.bcd.migrate.model;

import nl.naturalis.bcd.migrate.SelfTransformer;
import nl.naturalis.bcd.model.Requester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NbcIndiener implements SelfTransformer<Requester> {

  private static final Logger LOG = LoggerFactory.getLogger(NbcIndiener.class);

  private String Indienercode;
  private String IndienerNaam;
  private String IndActief;

  @Override
  public Requester morph() {
    Requester requester = new Requester();
    requester.setRequesterName(IndienerNaam);
    String s = IndienerNaam.toUpperCase();
    if (s.startsWith("NIEUWE NAAM") || s.startsWith("ONBEKEND")) {
      LOG.warn("Aanvrager {} wordt als \"Inactief\" gemigreerd", IndienerNaam);
      requester.setActive(false);
    } else {
      requester.setActive("J".equals(IndActief));
    }
    return requester;
  }

  public String getIndienercode() {
    return Indienercode;
  }

  public void setIndienercode(String indienercode) {
    Indienercode = indienercode;
  }

  public String getIndienerNaam() {
    return IndienerNaam;
  }

  public void setIndienerNaam(String indienerNaam) {
    IndienerNaam = indienerNaam;
  }

  public String getIndActief() {
    return IndActief;
  }

  public void setIndActief(String indActief) {
    IndActief = indActief;
  }
}
