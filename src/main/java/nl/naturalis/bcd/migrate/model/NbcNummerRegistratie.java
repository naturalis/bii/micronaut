package nl.naturalis.bcd.migrate.model;

import java.time.LocalDateTime;
import nl.naturalis.bcd.migrate.MigrationException;
import nl.naturalis.bcd.migrate.SelfTransformer;
import nl.naturalis.bcd.model.NumberSeries;
import nl.naturalis.bcd.model.RequestStatus;
import static nl.naturalis.common.ObjectMethods.e2n;
import static nl.naturalis.common.ObjectMethods.ifNull;

public class NbcNummerRegistratie implements SelfTransformer<NumberSeries> {
  private int RegistratieID;
  private int TypeVerzoekID;
  private String Indienercode;
  private String Afdelingcode;
  private int Hoeveelheid;
  private LocalDateTime DatumIndiening;
  private LocalDateTime DatumUitvoering;
  private int Eerstenummerreeks;
  private int Laatstenummerreeks;
  private String Omschrijvingverzoek;
  private int PrefixID;
  private String Statuscode;
  private String DeleteFlag;
  private int OmgevingID;
  private String Afdrukken;
  private String Topdesknummer;

  public NumberSeries morph() throws MigrationException {
    NumberSeries ns = new NumberSeries();
    ns.setAmount(Hoeveelheid);
    ns.setCmsId(OmgevingID);
    ns.setCollectionId(PrefixID);
    ns.setComments(Omschrijvingverzoek);
    ns.setDateCreated(DatumIndiening);
    ns.setDateFirstClosed(DatumUitvoering);
    ns.setDateModified(ifNull(DatumUitvoering, DatumIndiening));
    ns.setDispenseCount(1);
    ns.setFirstNumber(Eerstenummerreeks);
    ns.setLastNumber(Laatstenummerreeks);
    ns.setModifiedBy(0);
    ns.setTopDeskId(e2n(Topdesknummer));
    ns.setDispenseCount(DatumUitvoering == null ? 0 : 1);
    ns.setStatus(RequestStatus.CLOSED);
    return ns;
  }

  public int getRegistratieID() {
    return RegistratieID;
  }

  public void setRegistratieID(int registratieID) {
    RegistratieID = registratieID;
  }

  public int getTypeVerzoekID() {
    return TypeVerzoekID;
  }

  public void setTypeVerzoekID(int typeVerzoekID) {
    TypeVerzoekID = typeVerzoekID;
  }

  public String getIndienercode() {
    return Indienercode;
  }

  public void setIndienercode(String indienercode) {
    Indienercode = indienercode;
  }

  public String getAfdelingcode() {
    return Afdelingcode;
  }

  public void setAfdelingcode(String afdelingcode) {
    Afdelingcode = afdelingcode;
  }

  public int getHoeveelheid() {
    return Hoeveelheid;
  }

  public void setHoeveelheid(int hoeveelheid) {
    Hoeveelheid = hoeveelheid;
  }

  public LocalDateTime getDatumIndiening() {
    return DatumIndiening;
  }

  public void setDatumIndiening(LocalDateTime datumIndiening) {
    DatumIndiening = datumIndiening;
  }

  public LocalDateTime getDatumUitvoering() {
    return DatumUitvoering;
  }

  public void setDatumUitvoering(LocalDateTime datumUitvoering) {
    DatumUitvoering = datumUitvoering;
  }

  public int getEerstenummerreeks() {
    return Eerstenummerreeks;
  }

  public void setEerstenummerreeks(int eerstenummerreeks) {
    Eerstenummerreeks = eerstenummerreeks;
  }

  public int getLaatstenummerreeks() {
    return Laatstenummerreeks;
  }

  public void setLaatstenummerreeks(int laatstenummerreeks) {
    Laatstenummerreeks = laatstenummerreeks;
  }

  public String getOmschrijvingverzoek() {
    return Omschrijvingverzoek;
  }

  public void setOmschrijvingverzoek(String omschrijvingverzoek) {
    Omschrijvingverzoek = omschrijvingverzoek;
  }

  public int getPrefixID() {
    return PrefixID;
  }

  public void setPrefixID(int prefixID) {
    PrefixID = prefixID;
  }

  public String getStatuscode() {
    return Statuscode;
  }

  public void setStatuscode(String statuscode) {
    Statuscode = statuscode;
  }

  public String getDeleteFlag() {
    return DeleteFlag;
  }

  public void setDeleteFlag(String deleteFlag) {
    DeleteFlag = deleteFlag;
  }

  public int getOmgevingID() {
    return OmgevingID;
  }

  public void setOmgevingID(int omgevingID) {
    OmgevingID = omgevingID;
  }

  public String getAfdrukken() {
    return Afdrukken;
  }

  public void setAfdrukken(String afdrukken) {
    Afdrukken = afdrukken;
  }

  public String getTopdesknummer() {
    return Topdesknummer;
  }

  public void setTopdesknummer(String topdesknummer) {
    Topdesknummer = topdesknummer;
  }
}
