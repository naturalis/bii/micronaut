package nl.naturalis.bcd.model;

import nl.naturalis.common.check.Check;
import static nl.naturalis.common.ObjectMethods.e2n;
import static nl.naturalis.common.ObjectMethods.ifNotNull;

public class BcdCollection {

  public static String getPrefix(String instituteCode, String collectionCode) {
    Check.notNull(instituteCode, "instituteCode");
    if (collectionCode == null
        || collectionCode.isBlank()
        || collectionCode.equalsIgnoreCase(instituteCode)
        || collectionCode.equalsIgnoreCase("n.v.t.")
        || collectionCode.equalsIgnoreCase("n/a")) {
      return instituteCode;
    }
    return instituteCode + "." + collectionCode;
  }

  public static String getNormalizedCollectionCode(String instituteCode, String collectionCode) {
    Check.notNull(instituteCode, "instituteCode");
    if (collectionCode == null
        || collectionCode.isBlank()
        || collectionCode.equalsIgnoreCase(instituteCode)
        || collectionCode.equalsIgnoreCase("n.v.t.")
        || collectionCode.equalsIgnoreCase("n/a")) {
      return null;
    }
    return collectionCode;
  }

  private Integer collectionId;
  private Integer instituteId;
  private String collectionName;
  private String collectionCode;
  private String description;
  private boolean active = true;

  // Denormalizations
  private String instituteCode;

  public BcdCollection() {}

  public String prefix() {
    if (collectionId == null) {
      // This is a fresh instance, not populated yet with database or form data
      return null;
    }
    return getPrefix(getInstituteCode(), getCollectionCode());
  }

  public Integer getCollectionId() {
    return collectionId;
  }

  public void setCollectionId(Integer collectionId) {
    this.collectionId = collectionId;
  }

  public Integer getInstituteId() {
    return instituteId;
  }

  public void setInstituteId(Integer instituteId) {
    this.instituteId = instituteId;
  }

  public String getCollectionName() {
    return e2n(ifNotNull(collectionName, String::strip));
  }

  public void setCollectionName(String collectionName) {
    this.collectionName = collectionName;
  }

  public String getCollectionCode() {
    if (instituteCode == null) {
      return collectionCode;
    }
    return getNormalizedCollectionCode(instituteCode, collectionCode);
  }

  public void setCollectionCode(String collectionCode) {
    this.collectionCode = collectionCode;
  }

  public String getDescription() {
    return e2n(ifNotNull(description, String::strip));
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  public String getInstituteCode() {
    return e2n(ifNotNull(instituteCode, String::strip));
  }

  public void setInstituteCode(String instituteCode) {
    this.instituteCode = instituteCode;
  }
}
