package nl.naturalis.bcd.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import nl.naturalis.common.util.EnumParser;

public enum LayoutInputType {
  NUMBER_SERIES("Nummerreeks", NumberSeries.class),
  STORAGE_LOCATION("Storage Location", StorageLocation.class);

  private static final EnumParser<LayoutInputType> ep = new EnumParser<>(LayoutInputType.class);

  @JsonCreator
  public static LayoutInputType parse(Object val) {
    return ep.parse(val);
  }

  private String description;
  private Class<?> type;

  LayoutInputType(String description, Class<?> type) {
    this.description = description;
    this.type = type;
  }

  public String getDescription() {
    return description;
  }

  public Class<?> getType() {
    return type;
  }

  @JsonValue
  @Override
  public String toString() {
    return description;
  }
}
