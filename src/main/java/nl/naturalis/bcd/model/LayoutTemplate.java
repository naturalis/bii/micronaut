package nl.naturalis.bcd.model;

import com.fasterxml.jackson.annotation.JsonValue;

public enum LayoutTemplate {
  STANDARD("Standaard"),
  STORAGE_LOCATION("Storage Location"),
  LABORATORY("Laboratorium"),
  STD_PLUS("Standaard Plus (vrije tekst)");

  private final String description;

  LayoutTemplate(String descr) {
    this.description = descr;
  }

  @JsonValue
  public String toString() {
    return description;
  }
}
