package nl.naturalis.bcd.model;

public enum Permission {
    EDIT,
    VIEW
}
