package nl.naturalis.bcd.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import nl.naturalis.common.util.EnumParser;

public enum RequestStatus {
  OPEN("Open"),
  LOCKED("Vergrendeld"),
  CLOSED("Afgehandeld");

  private static final EnumParser<RequestStatus> ep =
      new EnumParser<>(RequestStatus.class);

  @JsonCreator
  public static RequestStatus parse(Object value) {
    return ep.parse(value);
  }

  private final String name;

  private RequestStatus(String name) {
    this.name = name;
  }

  @JsonValue
  public int intValue() {
    return ordinal();
  }

  public RequestStatus next() {
    return RequestStatus.class.getEnumConstants()[ordinal() + 1];
  }

  public RequestStatus previous() {
    return RequestStatus.class.getEnumConstants()[ordinal() - 1];
  }

  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    return name;
  }
}
