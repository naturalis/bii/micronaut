package nl.naturalis.bcd.model;

import static nl.naturalis.common.ObjectMethods.e2n;
import static nl.naturalis.common.ObjectMethods.ifNotNull;

public class Requester {

  private Integer requesterId;
  private String requesterName;
  private boolean active = true;

  public Requester() {}

  public Integer getRequesterId() {
    return requesterId;
  }

  public void setRequesterId(Integer requesterId) {
    this.requesterId = requesterId;
  }

  public String getRequesterName() {
    return e2n(ifNotNull(requesterName, String::strip));
  }

  public void setRequesterName(String requesterName) {
    this.requesterName = requesterName;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }
}
