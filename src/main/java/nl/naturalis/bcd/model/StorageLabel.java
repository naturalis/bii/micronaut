package nl.naturalis.bcd.model;

import nl.naturalis.bcd.exception.BcdUserInputException;
import nl.naturalis.common.check.Check;
import static nl.naturalis.common.StringMethods.count;
import static nl.naturalis.common.StringMethods.lchop;
import static nl.naturalis.common.StringMethods.substrTo;
import static nl.naturalis.common.StringMethods.trim;
import static nl.naturalis.common.check.CommonChecks.empty;

public class StorageLabel {

  private Integer storageLocationId;
  /*
   * The first few segments of the label, containing a coarse-grained location identifier (e.g. a
   * building)
   */
  private String coarseGrainedLocation;
  /*
   * The segments following the prefix segments, identifying the exact shelf (or so) that gets the
   * sticker with this label. This is actually nor a real number but rather a number-ish string like
   * "001.012"
   */
  private String fineGrainedLocation;

  public StorageLabel() {}

  public StorageLabel(String label, int prefixSegmentCount, int lineNo) {

    // NB The minimally required number of segments is actually one
    // more than the prefixSegmentCount, otherwise we would just have
    // a prefix and nothing else. But this will work out fine.

    // This counts the number of dots, **not** the number of segments.
    // The number of dots is always one less than the number of
    // segments (as they are in between the segments).
    int actual = count(label, '.', prefixSegmentCount);

    if (actual < prefixSegmentCount) {
      String fmt = "Regel %d (%s): aantal segmenten: %d. Minimum is %d.";
      String err = String.format(fmt, lineNo + 1, label, actual + 1, prefixSegmentCount + 1);
      throw new BcdUserInputException(err);
    }
    coarseGrainedLocation = substrTo(label, '.', prefixSegmentCount);
    fineGrainedLocation = lchop(label, coarseGrainedLocation + '.');
    Check.on(BcdUserInputException::new, trim(coarseGrainedLocation, ". \t\n"))
        .isNot(empty(), "Regel %d (%s): ongeldig label.");
    Check.on(BcdUserInputException::new, trim(fineGrainedLocation, ". \t\n"))
        .isNot(empty(), "Regel %d (%s): ongeldig label.");
  }

  public Integer getStorageLocationId() {
    return storageLocationId;
  }

  public void setStorageLocationId(Integer storageLocationId) {
    this.storageLocationId = storageLocationId;
  }

  public String getCoarseGrainedLocation() {
    return coarseGrainedLocation;
  }

  public void setCoarseGrainedLocation(String coarseGrainedLocation) {
    this.coarseGrainedLocation = coarseGrainedLocation;
  }

  public String getFineGrainedLocation() {
    return fineGrainedLocation;
  }

  public void setFineGrainedLocation(String fineGrainedLocation) {
    this.fineGrainedLocation = fineGrainedLocation;
  }

  @Override
  public String toString() {
    return coarseGrainedLocation + '.' + fineGrainedLocation;
  }
}
