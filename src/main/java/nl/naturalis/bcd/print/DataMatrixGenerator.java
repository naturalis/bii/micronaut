package nl.naturalis.bcd.print;

import nl.naturalis.bcd.BcdServer;
import org.apache.commons.codec.binary.Base64;
import org.krysalis.barcode4j.impl.datamatrix.DataMatrixBean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static java.awt.image.BufferedImage.TYPE_BYTE_BINARY;
import static org.krysalis.barcode4j.impl.datamatrix.SymbolShapeHint.FORCE_SQUARE;

public class DataMatrixGenerator {

  private static final Logger LOG = LoggerFactory.getLogger(BcdServer.class);

  private static final ThreadLocal<ByteArrayOutputStream> OUT =
      ThreadLocal.withInitial(() -> new ByteArrayOutputStream(255));

  // Image resolution (dots-per-inch)
  private static final int DPI = 200;
  private static final String MIME = "image/png";

  public static byte[] getBytes(String text) throws IOException {
    ByteArrayOutputStream out = OUT.get();
    out.reset();
    DataMatrixBean bean = new DataMatrixBean();
    bean.setModuleWidth(UnitConv.in2mm(8.0f / DPI));
    bean.doQuietZone(false);
    bean.setShape(FORCE_SQUARE);
    BitmapCanvasProvider canvas =
        new BitmapCanvasProvider(out, MIME, DPI, TYPE_BYTE_BINARY, false, 0);
    bean.generateBarcode(canvas, text);
    canvas.finish();
    return out.toByteArray();
  }

  private final String prefixWithDot;

  public DataMatrixGenerator(String prefixWithDot) {
    this.prefixWithDot = prefixWithDot;
  }

  public String getBase64EncodedBytes(int number) throws IOException {
    return new String(getBytes(number), StandardCharsets.UTF_8);
  }

  public byte[] getBytes(int number) throws IOException {
    ByteArrayOutputStream out = OUT.get();
    out.reset();
    DataMatrixBean bean = new DataMatrixBean();
    bean.setModuleWidth(UnitConv.in2mm(8.0f / DPI));
    bean.doQuietZone(false);
    bean.setShape(FORCE_SQUARE);
    BitmapCanvasProvider canvas =
        new BitmapCanvasProvider(out, "image/jpeg", DPI, TYPE_BYTE_BINARY, false, 0);
    bean.generateBarcode(canvas, prefixWithDot + number);
    canvas.finish();
    return out.toByteArray();
  }

  public String getBase64EncodedBytes2(int number) throws Exception {
    BufferedImage img = createDataMatrix(number);
    ByteArrayOutputStream out = OUT.get();
    out.reset();
    ImageIO.write(img, "jpeg", out);
    byte[] bytes = Base64.encodeBase64(out.toByteArray());
    return new String(bytes, StandardCharsets.UTF_8);
  }

  private BufferedImage createDataMatrix(int number) throws Exception {
    final int dpi = 200;
    DataMatrixBean bean = new DataMatrixBean();
    bean.setModuleWidth(UnitConv.in2mm(8.0f / dpi));
    bean.doQuietZone(false);
    bean.setShape(FORCE_SQUARE);
    ByteArrayOutputStream out = OUT.get();
    out.reset();
    BitmapCanvasProvider canvas =
        new BitmapCanvasProvider(out, "image/jpeg", DPI, TYPE_BYTE_BINARY, false, 0);
    bean.generateBarcode(canvas, prefixWithDot + number);
    canvas.finish();
    return canvas.getBufferedImage();
  }
}
