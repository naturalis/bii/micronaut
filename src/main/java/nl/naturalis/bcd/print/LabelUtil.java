package nl.naturalis.bcd.print;

import java.io.IOException;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import nl.naturalis.common.ExceptionMethods;

public class LabelUtil {

  public static final double MM_PER_INCH = 25.4;
  public static final double INCH_PER_MM = 1 / MM_PER_INCH;

  public static final double POINTS_PER_INCH = 72D;
  public static final double POINTS_PER_MM = POINTS_PER_INCH / MM_PER_INCH;

  public static final PDFont LABEL_FONT = PDType1Font.HELVETICA;
  public static final double LABEL_FONT_DIGIT_WIDTH = getHelveticaDigitWidth();

  /**
   * The factor by which we have to multiply all font sizes in order to make them vertically occupy
   * a given height. This takes into account that label text is all-caps. Thus there are no letters
   * sticking out beneath the baseline (like g or j). So, given a required height (say 10px) the
   * font size should actually be greater than 10px.
   *
   * <p>Unfortunately, whatever arithmetic we apply to whatever combi of font metrics (bounding box,
   * cap height and descent), things just won't add up to produce a factor by which to multiply a
   * font size such that an all-caps text occupies a certain height, so we might well hard code the
   * darn thing.
   */
  public static final double FS_SCALE_FACTOR = 1.455D;

  /**
   * There is something like a purely relative space in which font-related artifacts are defined
   * (e.g. with the width of the letter 'M' having a fixed ratio - given a font - to the width of
   * the letter 'i'). If you divide a relative width or height by 1000 and then multiply it by the
   * font size (in whatever unit of measure), you get the absolute width of the text - in that same
   * unit of measure.
   */
  public static double getRelativeStringWidth(String text) throws IOException {
    return LABEL_FONT.getStringWidth(text);
  }

  /**
   * Copied from be.quodlibet.boxable.utils.FontUtils. All other (similar) methods derive from this
   * one. It returns the absolute width (given the unit of measure used for the font size) of a
   * given string. Thus, if the font size is expressed in millimeters, you get the width in
   * millimeters of the specified text.
   */
  public static double getAbsoluteStringWidth(String text, double fontSize) throws IOException {
    return (fontSize * getRelativeStringWidth(text)) / 1000D;
  }

  /** So that's what we do. */
  public static double getAbsoluteMeasure(double relativeMeasure, double fontSize) {
    return (relativeMeasure * fontSize) / 1000D;
  }

  /**
   * Figures out the font size for a chunk of text with the specified relative width, given that it
   * should occupy all of the specified absolute width (e.g. in millimeters).
   */
  public static double getFontSizeForWidth(double relativeTextWidth, double absoluteTargetWidth) {
    // absoluteWidth = relativeWidth * fontSize / 1000
    // absoluteWidth / relativeWidth == fontSize / 1000
    // 1000 * absoluteWidth / relativeWidth == fontSize
    return 1000 * absoluteTargetWidth / relativeTextWidth;
  }

  public static double getFontSizeForWidth(
      double relativeTextWidth, double targetWidth, double maxFontSize) {
    return Math.min(maxFontSize, getFontSizeForWidth(relativeTextWidth, targetWidth));
  }

  /**
   * Returns the font size required to fully occupy all of the specified height given that label
   * text is in all-capitals.
   */
  public static double getFontSizeForHeight(double height) {
    return height * FS_SCALE_FACTOR;
  }

  public static double getHeightOccupiedBy(double fontSize) {
    return fontSize / FS_SCALE_FACTOR;
  }

  /**
   * Returns the width of a digit in the Helvetica font. For helvetica at least all digits (even
   * '1') have exactly the same width, contrary to for example 'i' and 'l' (thinner) and 'w' and 'W'
   * (wider). See LabelUtilTest.
   */
  private static double getHelveticaDigitWidth() {
    try {
      return LABEL_FONT.getWidth('0'); // Pick any digit; it's going to be 556.0
    } catch (IOException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }
}
