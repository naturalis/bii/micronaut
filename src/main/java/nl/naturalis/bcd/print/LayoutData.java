package nl.naturalis.bcd.print;

import nl.naturalis.bcd.model.Layout;
import nl.naturalis.bcd.model.NumberSeries;

/**
 * Base class for all classes providing the input for the label generation process. It is basically
 * a deserialized, validated and enriched version of the {@link Layout#getJsonConfig() jsonConfig}
 * column in the bcd_layout table.
 *
 * @author Ayco Holleman
 */
public abstract class LayoutData {

  protected PaperSize paperSize;
  protected double printAreaTop;
  protected double printAreaLeft;
  protected int rowCount;
  protected int columnCount;
  protected boolean columnWise;
  protected double rowHeight;
  protected double columnWidth;
  protected double imgSize;
  protected double numberEmphasis;
  protected boolean glueAll;
  protected boolean gluePrefix;
  private String[] suffixes;
  private Boolean suffixesOnly;

  // BEGIN COMPUTED VALUES

  public double getPageWidthMillimeters() {
    return paperSize.getWidth();
  }

  public double getPageHeightMillimeters() {
    return paperSize.getHeight();
  }

  public int labelsPerPage() {
    return columnCount * rowCount;
  }

  public int getPageCount(NumberSeries series) {
    return (int) Math.ceil((double) series.getAmount() / (double) (rowCount * columnCount));
  }

  public double getImgMarginRight() {
    return 0.088 * imgSize;
  }

  public boolean isSuffixesOnly() {
    return Boolean.TRUE.equals(suffixesOnly);
  }

  // END COMPUTED VALUES

  public PaperSize getPaperSize() {
    return paperSize;
  }

  public void setPaperSize(PaperSize paperSize) {
    this.paperSize = paperSize;
  }

  public double getPrintAreaTop() {
    return printAreaTop;
  }

  public void setPrintAreaTop(double printAreaTop) {
    this.printAreaTop = printAreaTop;
  }

  public double getPrintAreaLeft() {
    return printAreaLeft;
  }

  public void setPrintAreaLeft(double printAreaLeft) {
    this.printAreaLeft = printAreaLeft;
  }

  public int getRowCount() {
    return rowCount;
  }

  public void setRowCount(int rowCount) {
    this.rowCount = rowCount;
  }

  public int getColumnCount() {
    return columnCount;
  }

  public void setColumnCount(int columnCount) {
    this.columnCount = columnCount;
  }

  public boolean isColumnWise() {
    return columnWise;
  }

  public void setColumnWise(boolean columnWise) {
    this.columnWise = columnWise;
  }

  public double getRowHeight() {
    return rowHeight;
  }

  public void setRowHeight(double rowHeight) {
    this.rowHeight = rowHeight;
  }

  public double getColumnWidth() {
    return columnWidth;
  }

  public void setColumnWidth(double columnWidth) {
    this.columnWidth = columnWidth;
  }

  public double getImgSize() {
    return imgSize;
  }

  public void setImgSize(double imgSize) {
    this.imgSize = imgSize;
  }

  public double getNumberEmphasis() {
    return numberEmphasis;
  }

  public void setNumberEmphasis(double numberEmphasis) {
    this.numberEmphasis = numberEmphasis;
  }

  public boolean isGlueAll() {
    return glueAll;
  }

  public void setGlueAll(boolean glueAll) {
    this.glueAll = glueAll;
  }

  public boolean isGluePrefix() {
    return gluePrefix;
  }

  public void setGluePrefix(boolean gluePrefix) {
    this.gluePrefix = gluePrefix;
  }

  public String[] getSuffixes() {
    return suffixes;
  }

  public void setSuffixes(String[] suffixes) {
    this.suffixes = suffixes;
  }

  public Boolean getSuffixesOnly() {
    return suffixesOnly;
  }

  public void setSuffixesOnly(Boolean suffixesOnly) {
    this.suffixesOnly = suffixesOnly;
  }
}
