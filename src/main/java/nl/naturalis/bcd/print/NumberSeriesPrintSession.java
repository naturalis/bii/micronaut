package nl.naturalis.bcd.print;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.model.Layout;
import nl.naturalis.bcd.model.NumberSeries;
import nl.naturalis.bcd.model.StorageLocation;

/**
 * Base class for LabPrintSession (laboratory labels) and StdPrintSession (specimens and storage
 * units) as these are all fedd by a number series. It is not the base class for storage location
 * print sessions as the main input there is a {@link StorageLocation} instance.
 *
 * @author Ayco Holleman
 * @param <LAYOUT_DATA>
 * @param <LABEL>
 */
public abstract class NumberSeriesPrintSession<LAYOUT_DATA extends LayoutData, LABEL extends Label>
    extends PrintSessionBase<LAYOUT_DATA, LABEL> {

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(NumberSeriesPrintSession.class);

  private final NumberSeries series;

  private final String prefixWithDot;
  private final String instituteWithDot;
  private final String collectionWithDot;

  public NumberSeriesPrintSession(NumberSeries series, Layout layout) {
    super(layout);
    this.series = series;
    this.prefixWithDot = series.prefix() + '.';
    this.instituteWithDot = series.getInstituteCode() + '.';
    this.collectionWithDot = series.getCollectionCode() + '.';
  }

  public NumberSeries getNumberSeries() {
    return series;
  }

  public String getPrefixWithDot() {
    return prefixWithDot;
  }

  public String getInstituteWithDot() {
    return instituteWithDot;
  }

  public String getCollectionWithDot() {
    return collectionWithDot;
  }
}
