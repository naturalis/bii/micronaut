package nl.naturalis.bcd.print;

import org.apache.pdfbox.pdmodel.common.PDRectangle;
import com.fasterxml.jackson.annotation.JsonCreator;
import nl.naturalis.bcd.exception.BcdUserInputException;
import nl.naturalis.common.util.EnumParser;
import static nl.naturalis.bcd.print.LabelUtil.MM_PER_INCH;
import static nl.naturalis.common.ArrayMethods.implode;

/*
 * Symbolic constants for paper sizes. Only the ones that we currently support or that have a
 * reasonable chance of being required in the future.
 */
public enum PaperSize {
  A3(PDRectangle.A3, 297, 420),
  A4(PDRectangle.A4, 210, 297),
  LETTER(PDRectangle.LETTER, 8.5 * MM_PER_INCH, 11 * MM_PER_INCH);

  private static EnumParser<PaperSize> parser = new EnumParser<>(PaperSize.class);

  @JsonCreator
  public static PaperSize parse(Object value) {
    try {
      return parser.parse(value);
    } catch (IllegalArgumentException e) {
      String fmt = "Ongeldige waarde voor \"paperSize\": \"%s\". Geldige waardes: %s";
      String s = implode(PaperSize.class.getEnumConstants());
      String msg = String.format(fmt, value, s);
      throw new BcdUserInputException(msg);
    }
  }

  private final PDRectangle rectangle;
  private final double width;
  private final double height;

  private PaperSize(PDRectangle rectangle, double width, double height) {
    this.rectangle = rectangle;
    this.width = width;
    this.height = height;
  }

  public PDRectangle getRectangle() {
    return rectangle;
  }

  public double getWidth() {
    return width;
  }

  public double getHeight() {
    return height;
  }
}
