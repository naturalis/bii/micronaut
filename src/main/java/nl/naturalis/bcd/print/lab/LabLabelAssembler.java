package nl.naturalis.bcd.print.lab;

import java.math.BigDecimal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.print.DataMatrixGenerator;
import nl.naturalis.bcd.print.LabelAssembler;
import nl.naturalis.bcd.print.shared.FontSizeInfo;
import static java.math.RoundingMode.HALF_EVEN;
import static nl.naturalis.bcd.print.Label.COLUMN;
import static nl.naturalis.bcd.print.Label.ROW;
import static nl.naturalis.bcd.print.LabelUtil.POINTS_PER_MM;
import static nl.naturalis.bcd.print.SplitMode.DONT_SPLIT;
import static nl.naturalis.bcd.print.SplitMode.GLUE_PREFIX;
import static nl.naturalis.bcd.print.lab.LabLabel.*;
import static nl.naturalis.common.MathMethods.getPageRowColumn;
import static nl.naturalis.common.MathMethods.getPageRowColumnCM;
import static nl.naturalis.common.ObjectMethods.n2e;

class LabLabelAssembler extends LabelAssembler<LabLabel> {

  private static final Logger LOG = LoggerFactory.getLogger(LabLabelAssembler.class);

  private final LabPrintSession session;
  private final int cellIndex;
  private final int number;
  private final String suffix;

  LabLabelAssembler(LabPrintSession session, int cellIndex, int number, String suffix) {
    this.session = session;
    this.cellIndex = cellIndex;
    this.number = number;
    this.suffix = suffix;
  }

  @Override
  public LabLabel call() {
    LabPrintSession session = this.session;
    LabLayoutData layoutData = session.getLayoutData();
    int number = this.number;
    if (LOG.isTraceEnabled()) {
      LOG.trace("Creating label for {}{}", session.getPrefixWithDot(), number);
    }
    try {

      // Calculate the page, row and column that the cell finds itself in
      int[] cellInfo =
          layoutData.isColumnWise()
              ? getPageRowColumnCM(cellIndex, layoutData.getRowCount(), layoutData.getColumnCount())
              : getPageRowColumn(cellIndex, layoutData.getRowCount(), layoutData.getColumnCount());

      int row = cellInfo[ROW];
      int column = cellInfo[COLUMN];

      float[] layoutInfo = new float[LAYOUT_INFO_ITEM_COUNT];

      // HEADER
      if (row == 0) {
        // Left-align voucher header with data matrix
        layoutInfo[VOUCHER_HEADER_X] = toPoints(layoutData.getImageX(column));
        // Left-align subsample header with subsample text
        layoutInfo[SUBSAMPLE_HEADER_X] = toPoints(layoutData.getSubsampleX(column));
        layoutInfo[HEADER_Y] = toPoints(layoutData.getHeaderY());
        layoutInfo[HEADER_FONT_SIZE] = toPoints(layoutData.getHeaderFontSize());
      }

      // DATA MATRIX
      byte[] bytes =
          DataMatrixGenerator.getBytes(session.getPrefixWithDot() + number + n2e(suffix));

      layoutInfo[IMAGE_X] = toPoints(layoutData.getImageX(column));
      layoutInfo[IMAGE_Y] = toPoints(layoutData.getImageY(row));
      layoutInfo[IMAGE_SIZE] = toPoints(layoutData.getImgSize());

      // VOUCHER
      FontSizeInfo fontSizeInfo = session.getFontSizeInfoProvider().getFontSizeInfo(number, suffix);
      String line0;
      String line1;
      if (fontSizeInfo.getSplitMode() == DONT_SPLIT) {
        layoutInfo[VOUCHER_FONT_SIZE_LINE0] = toPoints(fontSizeInfo.getFontSizeLine0());
        layoutInfo[VOUCHER_TEXT_X] = toPoints(layoutData.getVoucherTextX(column));
        layoutInfo[VOUCHER_TEXT_LINE0_Y] =
            toPoints(layoutData.getVoucherTextLine0Y(row, fontSizeInfo));
        line0 = session.getPrefixWithDot() + number;
        line1 = null;
      } else {
        layoutInfo[VOUCHER_FONT_SIZE_LINE0] = toPoints(fontSizeInfo.getFontSizeLine0());
        layoutInfo[VOUCHER_FONT_SIZE_LINE1] = toPoints(fontSizeInfo.getFontSizeLine1());
        layoutInfo[VOUCHER_TEXT_X] = toPoints(layoutData.getVoucherTextX(column));
        layoutInfo[VOUCHER_TEXT_LINE0_Y] =
            toPoints(layoutData.getVoucherTextLine0Y(row, fontSizeInfo));
        layoutInfo[VOUCHER_TEXT_LINE1_Y] = toPoints(layoutData.getVoucherTextLine1Y(row));
        if (suffix == null) {
          if (fontSizeInfo.getSplitMode() == GLUE_PREFIX) {
            line0 = session.getPrefixWithDot();
            line1 = String.valueOf(number);
          } else {
            line0 = session.getInstituteWithDot();
            line1 = session.getCollectionWithDot() + number;
          }
        } else {
          line0 = session.getPrefixWithDot();
          line1 = String.valueOf(number) + suffix;
        }
      }

      // SUBSAMPLE
      double fontSize = session.getSubsampleFontSizeProvider().getFontSize(number, suffix);
      layoutInfo[SUBSAMPLE_FONT_SIZE] = toPoints(fontSize);
      layoutInfo[SUBSAMPLE_X] = toPoints(layoutData.getSubsampleX(column));
      layoutInfo[SUBSAMPLE_LINE_ABOVE_Y] = toPoints(layoutData.getSubsampleLineAboveY(row));
      layoutInfo[SUBSAMPLE_TEXT_Y] = toPoints(layoutData.getSubsampleTextY(row, fontSize));
      layoutInfo[SUBSAMPLE_LINE_BELOW_Y] =
          toPoints(layoutData.getSubsampleLineBelowY(row, fontSize));
      layoutInfo[SUBSAMPLE_LINE_END_X] =
          toPoints(layoutData.getSubsampleX(column) + layoutData.getSubsampleTextWidth());
      return LabLabel.create(number, suffix, bytes, cellInfo, layoutInfo, line0, line1);
    } catch (Throwable t) {
      return session.abort(t);
    }
  }

  private static float toPoints(double mm) {
    return new BigDecimal(mm * POINTS_PER_MM).setScale(8, HALF_EVEN).floatValue();
  }
}
