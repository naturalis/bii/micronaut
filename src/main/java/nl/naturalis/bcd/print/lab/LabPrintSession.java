package nl.naturalis.bcd.print.lab;

import java.util.Arrays;
import java.util.List;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.model.Layout;
import nl.naturalis.bcd.model.NumberSeries;
import nl.naturalis.bcd.print.NumberSeriesPrintSession;
import nl.naturalis.bcd.print.LabelWriter;
import nl.naturalis.bcd.print.LayoutDataFactory;
import nl.naturalis.bcd.print.shared.FontSizeComputer;
import nl.naturalis.bcd.print.shared.FontSizeInfoProvider;
import nl.naturalis.common.ArrayMethods;
import static nl.naturalis.common.ObjectMethods.ifNull;

public class LabPrintSession extends NumberSeriesPrintSession<LabLayoutData, LabLabel> {

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(LabPrintSession.class);

  private final FontSizeInfoProvider fontSizeInfoProvider;
  private final SubsampleFontSizeProvider subsampleFontSizeProvider;

  public LabPrintSession(NumberSeries series, Layout layout) {
    super(series, layout);
    double maxWidth = getLayoutData().getVoucherTextWidth();
    FontSizeComputer fsc = new FontSizeComputer(this, maxWidth);
    this.fontSizeInfoProvider = fsc.getFontSizeInfoProvider();
    SubsampleFontSizeComputer sfsc = new SubsampleFontSizeComputer(this);
    this.subsampleFontSizeProvider = sfsc.getFontSizeProvider();
  }

  @Override
  protected List<LabLabelAssembler> getLabelAssemblers() {
    NumberSeries ns = getNumberSeries();
    boolean suffixesOnly = getLayoutData().isSuffixesOnly();
    int init = suffixesOnly ? 0 : 1;
    String[] suffixes = ifNull(getLayoutData().getSuffixes(), ArrayMethods.EMPTY_STRING_ARRAY);
    int sz = (init + suffixes.length) * ns.getAmount();
    LabLabelAssembler[] assemblers = new LabLabelAssembler[sz];
    int i = 0;
    for (int nr = ns.getFirstNumber(); nr <= ns.getLastNumber(); ++nr) {
      if (!suffixesOnly) {
        assemblers[i] = new LabLabelAssembler(this, i, nr, null);
        ++i;
      }
      for (String s : suffixes) {
        assemblers[i] = new LabLabelAssembler(this, i, nr, s);
        ++i;
      }
    }
    return Arrays.asList(assemblers);
  }

  @Override
  protected LayoutDataFactory<LabLayoutData> getLayoutDataFactory(Layout layout) {
    return new LabLayoutDataFactory(layout);
  }

  @Override
  protected LabelWriter<LabLabel> createLabelWriter(PDPageContentStream pageContents) {
    return new LabLabelWriter(this, pageContents);
  }

  FontSizeInfoProvider getFontSizeInfoProvider() {
    return fontSizeInfoProvider;
  }

  SubsampleFontSizeProvider getSubsampleFontSizeProvider() {
    return subsampleFontSizeProvider;
  }
}
