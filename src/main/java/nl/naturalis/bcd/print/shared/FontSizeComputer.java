package nl.naturalis.bcd.print.shared;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.model.LayoutTemplate;
import nl.naturalis.bcd.model.NumberSeries;
import nl.naturalis.bcd.print.NumberSeriesPrintSession;
import nl.naturalis.bcd.print.LayoutData;
import nl.naturalis.bcd.print.SplitMode;
import nl.naturalis.common.ExceptionMethods;
import nl.naturalis.common.util.MutableDouble;
import static nl.naturalis.bcd.print.LabelUtil.*;
import static nl.naturalis.bcd.print.SplitMode.DONT_SPLIT;
import static nl.naturalis.bcd.print.SplitMode.GLUE_PREFIX;
import static nl.naturalis.bcd.print.SplitMode.SPLIT_PREFIX;
import static nl.naturalis.common.ObjectMethods.n2e;
import static nl.naturalis.common.StringMethods.EMPTY;
import static nl.naturalis.common.StringMethods.rpad;

/**
 * Computes font sizes for the {@link LayoutTemplate#STANDARD standard} and {@link
 * LayoutTemplate#LABORATORY laboratory} layout templates.
 *
 * @author Ayco Holleman
 */
public class FontSizeComputer {

  // The factor by which we will shrink the font sizes once we have computed them
  // to perfection. We need to do this when the text is to be split across two
  // lines because otherwise the letters from the 1st line would touch the letters
  // from the 2nd.
  public static final double SHRINK = 0.860;

  private static final Logger LOG = LoggerFactory.getLogger(FontSizeComputer.class);

  private NumberSeriesPrintSession<?, ?> session;

  // The width of the rectangle containing the label text. We need to shrink the font
  // size until the text fits inside it. If the font size has shrunk below half the
  // height of the rectangle, we jump from trying to fit the text into one line to
  // splitting the text in two and see if we can fit the two lines within the width
  // of the rectangle.
  private double maxWidth;

  public FontSizeComputer(NumberSeriesPrintSession<?, ?> session, double maxWidth) {
    this.session = session;
    this.maxWidth = maxWidth;
  }

  public FontSizeInfoProvider getFontSizeInfoProvider() {

    LOG.debug("Computing font sizes for {}", session.getNumberSeries());

    // The number of digits in the first number minus one. This will be used as the
    // *negative* offset into the FontSizeInfo[] array. For example:
    //
    // The array index for the FontSizeInfo object for number 845 (3 digits) in a
    // series starting with number 27 (2 digits) is:
    // (int) Math.log10(845) -  (int) Math.log10(27) ==  2 - 1 == 1
    //
    // The array index for the FontSizeInfo object for number 94 would be:
    // (int) Math.log10(94) -  (int) Math.log10(27) ==  1 - 1 == 0
    int offset = (int) Math.log10(session.getNumberSeries().getFirstNumber());

    Map<String, FontSizeInfo[]> fsInfoPerSuffix = new HashMap<>();
    try {
      // Use empty string as key for the default case (label without suffix). This
      // is safe because we have already verified that the user did not accidently
      // specify an empty suffix. Don't use null as key because the map is going
      // to be passed to Map.copyOf(map).
      fsInfoPerSuffix.put(EMPTY, computeFontSizes(null));
      if (session.getLayoutData().getSuffixes() != null) {
        for (String s : session.getLayoutData().getSuffixes()) {
          fsInfoPerSuffix.put(s, computeFontSizes(s));
        }
      }
      return new FontSizeInfoProvider(fsInfoPerSuffix, offset);
    } catch (IOException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  private FontSizeInfo[] computeFontSizes(String suffix) throws IOException {

    NumberSeries series = session.getNumberSeries();

    // The number of digits in the 1st number minus one
    int x = (int) Math.log10(series.getFirstNumber());
    int y = (int) Math.log10(series.getLastNumber());

    FontSizeInfo[] fsi = new FontSizeInfo[y - x + 1];
    for (int i = x; i <= y; ++i) {
      fsi[i - x] = computeFontSize(i + 1, suffix);
      if (LOG.isDebugEnabled()) {
        String fmt = "%s[%d digits]%s ";
        String s = String.format(fmt, session.getPrefixWithDot(), i + 1, n2e(suffix));
        s = rpad(s, 32, '.', ": ") + fsi[i - x];
        LOG.debug(s);
      }
    }
    return fsi;
  }

  private FontSizeInfo computeFontSize(int numDigits, String suffix) throws IOException {

    final double wPrefix = getRelativeStringWidth(session.getPrefixWithDot());
    final double wNumber = numDigits * LABEL_FONT_DIGIT_WIDTH;
    final double wSuffix = suffix == null ? 0 : getRelativeStringWidth(suffix);
    final double wText = wPrefix + wNumber + wSuffix;

    final LayoutData layoutData = session.getLayoutData();
    final double imgSize = layoutData.getImgSize();
    final boolean isAllCapsSuffix = isAllCaps(suffix);

    // The height of the data matrix, which is also the maximum height of the label text.
    // And therefore also the maximum font size, since font size is about the height of
    // a font, not its width.
    double maxFontSize = isAllCapsSuffix ? getFontSizeForHeight(imgSize) : imgSize;

    if (layoutData.isGlueAll()) {
      // Institute code, collection code, number and suffix must all be cramped onto a
      // single line, however small a font it takes
      double fontSize = getFontSizeForWidth(wText, maxWidth, maxFontSize);
      return new FontSizeInfo(fontSize, DONT_SPLIT);
    }

    // The relative importance of the number segement of the label text versus the prefix
    // segment, expressed as a percentage. For example, if numberEmphasis is 60, then the
    // line containing the number is allowed to take up 60% of the total height of the
    // text <div>. The actual percentage may turn out to be smaller if the corresponding
    // font size would cause the text on the second line to overflow.
    double numberEmphasis = layoutData.getNumberEmphasis();

    // The font size below which we need to split the label text across two lines.
    double tippingPoint = ((numberEmphasis / 100D) * maxFontSize);

    // First check if the entire label text fits on one line
    double fontSize = getFontSizeForWidth(wText, maxWidth, maxFontSize);
    if (fontSize > tippingPoint) {
      return new FontSizeInfo(fontSize, DONT_SPLIT);
    }

    // Too bad, we need to split the label text across two lines. Now the tipping point
    // becomes the maximum font size for the 2nd (!) line. The 1st line's maximum font
    // size will be whatever is left after calculating the actually possible font size
    // for the 2nd line.

    // If we have a suffix, we ditch all the subtleties: institute and collection will go
    // on the 1st line; number and suffix on the 2nd. It just gets too complicated
    // otherwise.
    if (suffix != null) {
      double shrink = getShrinkFactor(isAllCapsSuffix);
      maxFontSize = shrink * tippingPoint;
      double fontSize1 = getFontSizeForWidth(wNumber + wSuffix, maxWidth, maxFontSize);
      double leftOver = imgSize - getHeightOccupiedBy(fontSize1);
      maxFontSize = shrink * getFontSizeForHeight(leftOver);
      double fontSize0 = getFontSizeForWidth(wPrefix, maxWidth, maxFontSize);
      return new FontSizeInfo(fontSize0, fontSize1, GLUE_PREFIX);
    }

    AtomicReference<SplitMode> splitMode = new AtomicReference<>();
    MutableDouble wLineOne = new MutableDouble();
    MutableDouble wLineTwo = new MutableDouble();
    getSplitInfo(numDigits, splitMode, wLineOne, wLineTwo);

    maxFontSize = tippingPoint;
    double fontSize1 = getFontSizeForWidth(wLineTwo.get(), maxWidth, maxFontSize);
    double leftOver = imgSize - getHeightOccupiedBy(fontSize1);
    maxFontSize = getFontSizeForHeight(leftOver);
    double fontSize0 = getFontSizeForWidth(wLineOne.get(), maxWidth, maxFontSize);

    fontSize0 *= Math.min(SHRINK, (fontSize0 + fontSize1) / imgSize);
    fontSize1 *= Math.min(SHRINK, (fontSize0 + fontSize1) / imgSize);

    return new FontSizeInfo(fontSize0, fontSize1, splitMode.get());
  }

  private void getSplitInfo(
      int numDigits,
      AtomicReference<SplitMode> splitMode,
      MutableDouble wLineOne,
      MutableDouble wLineTwo)
      throws IOException {
    NumberSeriesPrintSession<?, ?> session = this.session;
    LayoutData layoutData = session.getLayoutData();
    NumberSeries series = session.getNumberSeries();
    double wPrefix = getRelativeStringWidth(session.getPrefixWithDot());
    double wNumber = numDigits * LABEL_FONT_DIGIT_WIDTH;
    if (layoutData.isGluePrefix() || series.getCollectionCode() == null) {
      splitMode.setPlain(GLUE_PREFIX);
      wLineOne.set(wPrefix);
      wLineTwo.set(wNumber);
    } else {
      double wInstitute = getRelativeStringWidth(session.getInstituteWithDot());
      double wRest = getRelativeStringWidth(session.getCollectionWithDot()) + wNumber;
      double r0 = wPrefix > wNumber ? wNumber / wPrefix : wPrefix / wNumber;
      double r1 = wInstitute > wRest ? wRest / wInstitute : wInstitute / wRest;
      // Choose the option where the ratio is closest to 1 so we can
      // keep the font size as high as possible
      if (r0 < r1) {
        splitMode.setPlain(SPLIT_PREFIX);
        wLineOne.set(wInstitute);
        wLineTwo.set(wRest);
      } else {
        splitMode.setPlain(GLUE_PREFIX);
        wLineOne.set(wPrefix);
        wLineTwo.set(wNumber);
      }
    }
  }

  // Returns the shrink factor in case we have a suffix. The default shrink factor is
  // based on the assumption that the label text is in uppercase letters and is tuned
  // to create a little bit of whitespace between the first line and the second line
  // using that assumption. Unbelievably, though, in helvetica some lowercase ltters
  // (like 'd') are actually taller than the uppercase letters (which always have the
  // same height). We are not going to be subtle and figure out if the suffix contains
  // any of those letters. Enough already. As soon as we detect a lowercase letter we
  // are going to apply an even stronger shrinking of the font sizes.
  private static double getShrinkFactor(boolean isAllCapsSuffix) {
    return isAllCapsSuffix ? SHRINK : 0.805;
  }

  private static boolean isAllCaps(String suffix) {
    if (suffix == null) {
      return true;
    }
    return suffix.codePoints().noneMatch(Character::isLowerCase);
  }
}
