package nl.naturalis.bcd.print.std;

import java.math.BigDecimal;
import java.util.concurrent.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.print.DataMatrixGenerator;
import nl.naturalis.bcd.print.LabelAssembler;
import nl.naturalis.bcd.print.shared.FontSizeInfo;
import nl.naturalis.common.ExceptionMethods;
import static java.math.RoundingMode.HALF_EVEN;
import static nl.naturalis.bcd.print.Label.COLUMN;
import static nl.naturalis.bcd.print.Label.ROW;
import static nl.naturalis.bcd.print.LabelUtil.POINTS_PER_MM;
import static nl.naturalis.bcd.print.SplitMode.DONT_SPLIT;
import static nl.naturalis.bcd.print.SplitMode.GLUE_PREFIX;
import static nl.naturalis.bcd.print.std.StdLabel.*;
import static nl.naturalis.common.MathMethods.getPageRowColumn;
import static nl.naturalis.common.MathMethods.getPageRowColumnCM;
import static nl.naturalis.common.ObjectMethods.n2e;

/**
 * A {@link Callable} implementation that gathers all information required to print a single label.
 *
 * @author Ayco Holleman
 */
class StdLabelAssembler extends LabelAssembler<StdLabel> {

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(StdLabelAssembler.class);

  private final StdPrintSession session;
  private final int cellIndex;
  private final int number;
  private final String suffix;

  StdLabelAssembler(StdPrintSession session, int cellIndex, int number, String suffix) {
    this.session = session;
    this.cellIndex = cellIndex;
    this.number = number;
    this.suffix = suffix;
  }

  @Override
  public StdLabel call() {
    StdPrintSession session = this.session;
    StdLayoutData layoutData = session.getLayoutData();
    int number = this.number;
    try {

      int[] cellInfo =
          layoutData.isColumnWise()
              ? getPageRowColumnCM(cellIndex, layoutData.getRowCount(), layoutData.getColumnCount())
              : getPageRowColumn(cellIndex, layoutData.getRowCount(), layoutData.getColumnCount());

      // DATA MATRIX
      byte[] bytes =
          DataMatrixGenerator.getBytes(session.getPrefixWithDot() + number + n2e(suffix));

      float[] layoutInfo = new float[LAYOUT_INFO_ITEM_COUNT];

      layoutInfo[IMAGE_X] = toPoints(layoutData.getImageX(cellInfo[COLUMN]));
      layoutInfo[IMAGE_Y] = toPoints(layoutData.getImageY(cellInfo[ROW]));
      layoutInfo[IMAGE_SIZE] = toPoints(layoutData.getImgSize());

      // LABEL TEXT
      FontSizeInfo fontSizeInfo = session.getFontSizeInfoProvider().getFontSizeInfo(number, suffix);
      String line0;
      String line1;
      if (fontSizeInfo.getSplitMode() == DONT_SPLIT) {
        layoutInfo[FONT_SIZE_LINE0] = toPoints(fontSizeInfo.getFontSizeLine0());
        layoutInfo[TEXT_X] = toPoints(layoutData.getTextX(cellInfo[COLUMN]));
        layoutInfo[TEXT_LINE0_Y] = toPoints(layoutData.getTextLine0Y(cellInfo[ROW], fontSizeInfo));
        line0 = session.getPrefixWithDot() + number + n2e(suffix);
        line1 = null;
      } else {
        layoutInfo[FONT_SIZE_LINE0] = toPoints(fontSizeInfo.getFontSizeLine0());
        layoutInfo[FONT_SIZE_LINE1] = toPoints(fontSizeInfo.getFontSizeLine1());
        layoutInfo[TEXT_X] = toPoints(layoutData.getTextX(cellInfo[COLUMN]));
        layoutInfo[TEXT_LINE0_Y] = toPoints(layoutData.getTextLine0Y(cellInfo[ROW], fontSizeInfo));
        layoutInfo[TEXT_LINE1_Y] = toPoints(layoutData.getTextLine1Y(cellInfo[ROW]));
        if (suffix == null) {
          if (fontSizeInfo.getSplitMode() == GLUE_PREFIX) {
            line0 = session.getPrefixWithDot();
            line1 = String.valueOf(number);
          } else {
            line0 = session.getInstituteWithDot();
            line1 = session.getCollectionWithDot() + number;
          }
        } else {
          line0 = session.getPrefixWithDot();
          line1 = String.valueOf(number) + suffix;
        }
      }
      return StdLabel.create(number, suffix, bytes, cellInfo, layoutInfo, line0, line1);
    } catch (Throwable t) {
      session.abort(t);
      throw ExceptionMethods.uncheck(t);
    }
  }

  private static float toPoints(double mm) {
    return new BigDecimal(mm * POINTS_PER_MM).setScale(8, HALF_EVEN).floatValue();
  }
}
