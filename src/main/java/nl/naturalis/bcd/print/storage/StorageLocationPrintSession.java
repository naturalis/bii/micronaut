package nl.naturalis.bcd.print.storage;

import java.util.Arrays;
import java.util.List;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.model.Layout;
import nl.naturalis.bcd.model.StorageLabel;
import nl.naturalis.bcd.model.StorageLocation;
import nl.naturalis.bcd.print.LabelWriter;
import nl.naturalis.bcd.print.LayoutDataFactory;
import nl.naturalis.bcd.print.PrintSessionBase;

public class StorageLocationPrintSession extends PrintSessionBase<SLLayoutData, SLLabel> {

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(StorageLocationPrintSession.class);

  private final StorageLocation storageLocation;

  public StorageLocationPrintSession(StorageLocation storageLocation, Layout layout) {
    super(layout);
    this.storageLocation = storageLocation;
  }

  protected List<SLLabelAssembler> getLabelAssemblers() {
    int sz = storageLocation.getLabels().size();
    SLLabelAssembler[] assemblers = new SLLabelAssembler[sz];
    int i = 0;
    for (StorageLabel sl : storageLocation.getLabels()) {
      assemblers[i] = new SLLabelAssembler(this, i, sl);
      ++i;
    }
    return Arrays.asList(assemblers);
  }

  protected LayoutDataFactory<SLLayoutData> getLayoutDataFactory(Layout layout) {
    return new SLLayoutDataFactory(layout);
  }

  protected LabelWriter<SLLabel> createLabelWriter(PDPageContentStream pageContents) {
    return new SLLabelWriter(this, pageContents);
  }
}
