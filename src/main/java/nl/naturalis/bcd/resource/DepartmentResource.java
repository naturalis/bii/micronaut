package nl.naturalis.bcd.resource;

import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import nl.naturalis.bcd.model.Permission;
import org.klojang.template.RenderException;
import org.klojang.template.RenderSession;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.jax.rs.annotations.Pac4JProfileManager;
import org.pac4j.jax.rs.annotations.Pac4JSecurity;
import org.pac4j.oidc.profile.azuread.AzureAdProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.dao.DepartmentDao;
import nl.naturalis.bcd.exception.BcdUserInputException;
import nl.naturalis.bcd.exception.NotFoundException;
import nl.naturalis.bcd.json.BcdModule;
import nl.naturalis.bcd.json.JsonParam;
import nl.naturalis.bcd.model.Department;
import nl.naturalis.bcd.search.DepartmentSearchRequest;
import nl.naturalis.bcd.search.DepartmentSortColumn;
import nl.naturalis.bcd.search.Entity;
import nl.naturalis.bcd.search.SeriesSearchRequest;
import static nl.naturalis.bcd.resource.ResourceUtil.getEditHeader;
import static nl.naturalis.bcd.resource.ResourceUtil.stream;
import static nl.naturalis.bcd.search.Entity.DEPARTMENT;
import static nl.naturalis.bcd.search.SortOrder.ASC;
import static nl.naturalis.common.ObjectMethods.ifEmpty;
import static nl.naturalis.common.StringMethods.ellipsis;

@Path("/department")
@Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeAdmin")
@Produces(MediaType.TEXT_HTML)
public class DepartmentResource extends CRUDResource<Department, DepartmentDao> {

  private static final Logger LOG = LoggerFactory.getLogger(DepartmentResource.class);

  public DepartmentResource() {
    super(new DepartmentDao(), Department.class);
  }

  @GET
  @Produces(MediaType.TEXT_HTML)
  public StreamingOutput list(
      @QueryParam("searchRequest") JsonParam<DepartmentSearchRequest> jsonParam,
      @Context HttpServletRequest httpRequest,
      @Pac4JProfileManager ProfileManager<AzureAdProfile> pm)
      throws RenderException {
    this.pm = pm;
    this.profile = getProfile(pm);
    if (jsonParam == null) {
      jsonParam = new JsonParam<>();
    }
    DepartmentSearchRequest sr = jsonParam.deserialize(DepartmentSearchRequest.class);
    if (sr.getSortColumn() == null) {
      sr.setSortColumn(DepartmentSortColumn.DEPARTMENT_NAME);
    }
    if (sr.getSortOrder() == null) {
      sr.setSortOrder(ASC);
    }
    return super.list(sr, httpRequest, pm);
  }

  @GET
  @Path("/show-requests")
  @Produces(MediaType.TEXT_HTML)
  public Response showRequests(
          @QueryParam("id") int entityId,
          @QueryParam("name") String name,
          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm) {
    // Subtype of SearchRequest doesn't matter
    return showRequests(new SeriesSearchRequest(), Entity.DEPARTMENT, entityId, name, pm);
  }

  @GET
  @Path("/new")
  @Produces(MediaType.TEXT_HTML)
  public Response create(
          @Context HttpServletRequest httpRequest,
          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm)
          throws RenderException {
    return super.create(Department::new, httpRequest, pm);
  }

  @Override
  void beforeRenderCreatePage(RenderSession session) throws RenderException {
    session.set("header", "Nieuwe Afdeling Invoeren");
  }

  @GET
  @Path("{id}/edit")
  @Produces(MediaType.TEXT_HTML)
  public Response edit(@PathParam("id") int id,
                       @Context HttpServletRequest httpRequest,
                       @Pac4JProfileManager ProfileManager<AzureAdProfile> pm)
      throws RenderException {
    this.pm = pm;
    this.profile = getProfile(pm);
    LOG.debug("edit (type={};id={})", typeName(), id);
    String searchRequest = ifEmpty(httpRequest.getParameter("searchRequest"), "{}");
    Optional<Department> opt = dao.find(Department.class, Department::new, id);
    if (opt.isEmpty()) {
      throw new NotFoundException(DEPARTMENT, id);
    }
    Department bean = opt.get();
    RenderSession session = newEditPageRenderSession();
    Permission permission = hasPermission();
    session
        .set("header", getEditHeader(DEPARTMENT, ellipsis(bean.getDepartmentName(), 20)))
        .set("modelBean", BcdModule.writeJson(bean))
        .set("basePath", getBasePath(httpRequest))
        .set("searchRequest", searchRequest)
        .set("permission", permission.name())
        .set("hasRequests", dao.hasRequests(id))
        .showRecursive();
    return stream(session::render);
  }

  @GET
  @Path("/save-or-update")
  public Response saveOrUpdate(@Context HttpServletRequest httpRequest,
                               @Pac4JProfileManager ProfileManager<AzureAdProfile> pm) {
    return super.saveOrUpdate(httpRequest, Department::getDepartmentId, pm);
  }

  @DELETE
  @Path("/{id}")
  public Response delete(@PathParam("id") int id) {
    return super.delete(id);
  }

  void beforeDelete(int id) {
    if (dao.hasRequests(id)) {
      throw new BcdUserInputException("Er zijn nog nummerreeksen aan deze afdeling gekoppeld");
    }
  }

  @POST
  @Path("/{id}/deactivate")
  public Response deactivate(@PathParam("id") int id) {
    return super.deactivate(id);
  }

  @POST
  @Path("/unique")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public boolean isUnique(Department department) {
    LOG.debug("isUniqueName (type={};name={})", typeName(), department.getDepartmentName());
    return dao.isUnique(department);
  }
}
