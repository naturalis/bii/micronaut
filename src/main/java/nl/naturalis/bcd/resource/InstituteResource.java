package nl.naturalis.bcd.resource;

import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import nl.naturalis.bcd.model.Permission;
import org.klojang.template.RenderException;
import org.klojang.template.RenderSession;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.jax.rs.annotations.Pac4JProfileManager;
import org.pac4j.jax.rs.annotations.Pac4JSecurity;
import org.pac4j.oidc.profile.azuread.AzureAdProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.dao.CollectionDao;
import nl.naturalis.bcd.dao.InstituteDao;
import nl.naturalis.bcd.exception.BcdUserException;
import nl.naturalis.bcd.exception.NotFoundException;
import nl.naturalis.bcd.json.BcdModule;
import nl.naturalis.bcd.json.JsonParam;
import nl.naturalis.bcd.model.Institute;
import nl.naturalis.bcd.search.Entity;
import nl.naturalis.bcd.search.InstituteSearchRequest;
import nl.naturalis.bcd.search.InstituteSortColumn;
import nl.naturalis.bcd.search.SeriesSearchRequest;
import static nl.naturalis.bcd.resource.ResourceUtil.getEditHeader;
import static nl.naturalis.bcd.resource.ResourceUtil.stream;
import static nl.naturalis.bcd.search.Entity.INSTITUTE;
import static nl.naturalis.bcd.search.SortOrder.ASC;
import static nl.naturalis.common.ObjectMethods.ifEmpty;

@Path("/institute")
@Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeAdmin")
public class InstituteResource extends CRUDResource<Institute, InstituteDao> {

  private static final Logger LOG = LoggerFactory.getLogger(InstituteResource.class);

  public InstituteResource() {
    super(new InstituteDao(), Institute.class);
  }

  @GET
  @Produces(MediaType.TEXT_HTML)
  public StreamingOutput list(
      @QueryParam("searchRequest") JsonParam<InstituteSearchRequest> jsonParam,
      @Context HttpServletRequest httpRequest,
      @Pac4JProfileManager ProfileManager<AzureAdProfile> pm)
      throws RenderException {
    this.pm = pm;
    this.profile = getProfile(pm);
    if (jsonParam == null) {
      jsonParam = new JsonParam<>();
    }
    InstituteSearchRequest sr = jsonParam.deserialize(InstituteSearchRequest.class);
    if (sr.getSortColumn() == null) {
      sr.setSortColumn(InstituteSortColumn.INSTITUTE_CODE);
    }
    if (sr.getSortOrder() == null) {
      sr.setSortOrder(ASC);
    }
    return super.list(sr, httpRequest, pm);
  }

  @GET
  @Path("/show-requests")
  @Produces(MediaType.TEXT_HTML)
  public Response showRequests(
          @QueryParam("id") int entityId,
          @QueryParam("name") String name,
          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm) {
    // Subtype of SearchRequest doesn't matter
    return showRequests(new SeriesSearchRequest(), Entity.INSTITUTE, entityId, name, pm);
  }

  @GET
  @Path("/new")
  @Produces(MediaType.TEXT_HTML)
  public Response create(
          @Context HttpServletRequest httpRequest,
          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm) throws RenderException {
    return super.create(Institute::new, httpRequest, pm);
  }

  @Override
  void beforeRenderCreatePage(RenderSession session) throws RenderException {
    session.set("header", "Nieuw Instituut Invoeren");
  }

  @GET
  @Path("{id}/edit")
  @Produces(MediaType.TEXT_HTML)
  public Response edit(@PathParam("id") int id,
                       @Context HttpServletRequest httpRequest,
                       @Pac4JProfileManager ProfileManager<AzureAdProfile> pm)
      throws RenderException {
    this.pm = pm;
    this.profile = getProfile(pm);
    LOG.debug("edit (type={};id={})", typeName(), id);
    String searchRequest = ifEmpty(httpRequest.getParameter("searchRequest"), "{}");
    Optional<Institute> opt = dao.find(Institute.class, Institute::new, id);
    if (opt.isEmpty()) {
      throw new NotFoundException(INSTITUTE, id);
    }
    Institute bean = opt.get();
    RenderSession session = newEditPageRenderSession();
    Permission permission = hasPermission();
    session
        .set("header", getEditHeader(INSTITUTE, bean.getInstituteCode()))
        .set("modelBean", BcdModule.writeJson(bean))
        .set("basePath", getBasePath(httpRequest))
        .set("searchRequest", searchRequest)
        .set("permission", permission.name())
        .set("hasRequests", dao.hasRequests(id))
        .showRecursive("adminNavbar", "EditButtons");
    return stream(session::render);
  }

  @GET
  @Path("/save-or-update")
  public Response saveOrUpdate(@Context HttpServletRequest httpRequest,
                               @Pac4JProfileManager ProfileManager<AzureAdProfile> pm) {
    return super.saveOrUpdate(httpRequest, Institute::getInstituteId, pm);
  }

  @DELETE
  @Path("/{id}")
  public Response delete(@PathParam("id") int id) {
    LOG.debug("delete (type={};id={})", typeName(), id);
    if (dao.hasRequests(id)) {
      throw new BcdUserException("Er zijn nog nummerreeksen aan dit instituut gekoppeld");
    }
    LOG.debug("Deleting collections where instituteId={}", id);
    CollectionDao cdao = new CollectionDao();
    int deleted = cdao.deleteByInstituteId(id);
    LOG.debug("{} collection(s) deleted", deleted);
    LOG.debug("Deleting {}", typeName());
    if (dao.delete(id) == 0) {
      LOG.warn("Nothing deleted");
    }
    return Response.noContent().build();
  }

  @POST
  @Path("/{id}/deactivate")
  public Response deactivate(@PathParam("id") int id) {
    return super.deactivate(id);
  }

  @POST
  @Path("/unique")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public boolean isUnique(Institute institute) {
    LOG.debug("isUnique(type={};name={})", typeName(), institute.getInstituteCode());
    return dao.isUnique(institute);
  }
}
