package nl.naturalis.bcd.search;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import nl.naturalis.common.util.EnumParser;

public enum CmsSortColumn {
  CMS_NAME("a.cmsName"),
  REQUEST_COUNT("requestCount"),
  NUMBER_COUNT("numberCount"),
  ACTIVE("a.active");

  private static final EnumParser<CmsSortColumn> ep = new EnumParser<>(CmsSortColumn.class);

  @JsonCreator
  public static CmsSortColumn parse(Object value) {
    return ep.parse(value);
  }

  private final String label;

  CmsSortColumn(String label) {
    this.label = label;
  }

  @JsonValue
  public String toString() {
    return name();
  }

  public String getLabel() {
    return label;
  }
}
