package nl.naturalis.bcd.search;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import nl.naturalis.common.util.EnumParser;

public enum CollectionSortColumn {
  COLLECTION_NAME("a.collectionName"),
  COLLECTION_CODE("a.collectionCode"),
  DESCRIPTION("a.description"),
  REQUEST_COUNT("requestCount"),
  MAX_NUMBER("maxNumber"),
  NUMBER_COUNT("numberCount"),
  ACTIVE("a.active");

  private static final EnumParser<CollectionSortColumn> ep =
      new EnumParser<>(CollectionSortColumn.class);

  @JsonCreator
  public static CollectionSortColumn parse(Object value) {
    return ep.parse(value);
  }

  private final String label;

  CollectionSortColumn(String label) {
    this.label = label;
  }

  @JsonValue
  public String toString() {
    return name();
  }

  public String getLabel() {
    return label;
  }
}
