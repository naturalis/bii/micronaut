package nl.naturalis.bcd.search;

public class DepartmentSearchRequest extends SearchRequest {

  private DepartmentSortColumn sortColumn;

  public DepartmentSortColumn getSortColumn() {
    return sortColumn;
  }

  public void setSortColumn(DepartmentSortColumn sortColumn) {
    this.sortColumn = sortColumn;
  }
}
