package nl.naturalis.bcd.search;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import nl.naturalis.common.util.EnumParser;

public enum DepartmentSortColumn {
  DEPARTMENT_NAME("a.departmentName"),
  REQUEST_COUNT("requestCount"),
  NUMBER_COUNT("numberCount"),
  ACTIVE("a.active");

  private static final EnumParser<DepartmentSortColumn> ep =
      new EnumParser<>(DepartmentSortColumn.class);

  @JsonCreator
  public static DepartmentSortColumn parse(Object value) {
    return ep.parse(value);
  }

  private final String label;

  DepartmentSortColumn(String label) {
    this.label = label;
  }

  @JsonValue
  public String toString() {
    return name();
  }

  public String getLabel() {
    return label;
  }
}
