package nl.naturalis.bcd.search;

public class InstituteSearchRequest extends SearchRequest {

  private InstituteSortColumn sortColumn;

  public InstituteSortColumn getSortColumn() {
    return sortColumn;
  }

  public void setSortColumn(InstituteSortColumn sortColumn) {
    this.sortColumn = sortColumn;
  }
}
