package nl.naturalis.bcd.search;

public class RequesterSearchRequest extends SearchRequest {

  private RequesterSortColumn sortColumn;

  public RequesterSortColumn getSortColumn() {
    return sortColumn;
  }

  public void setSortColumn(RequesterSortColumn sortColumn) {
    this.sortColumn = sortColumn;
  }
}
