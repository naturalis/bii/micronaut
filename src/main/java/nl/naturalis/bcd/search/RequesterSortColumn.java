package nl.naturalis.bcd.search;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import nl.naturalis.common.util.EnumParser;

public enum RequesterSortColumn {
  REQUESTER_NAME("a.requesterName"),
  REQUEST_COUNT("requestCount"),
  NUMBER_COUNT("numberCount"),
  ACTIVE("a.active");

  private static final EnumParser<RequesterSortColumn> ep =
      new EnumParser<>(RequesterSortColumn.class);

  @JsonCreator
  public static RequesterSortColumn parse(Object value) {
    return ep.parse(value);
  }

  private final String label;

  RequesterSortColumn(String label) {
    this.label = label;
  }

  @JsonValue
  public String toString() {
    return name();
  }

  public String getLabel() {
    return label;
  }
}
