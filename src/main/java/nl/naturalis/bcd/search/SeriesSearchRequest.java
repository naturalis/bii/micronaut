package nl.naturalis.bcd.search;

public class SeriesSearchRequest extends SearchRequest {

  private SeriesSortColumn sortColumn;
  private boolean pendingOnly;
  private boolean mineOnly;

  public SeriesSearchRequest() {}

  public SeriesSearchRequest(SeriesSearchRequest other) {
    super(other);
    this.sortColumn = other.sortColumn;
    this.pendingOnly = other.pendingOnly;
    this.mineOnly = other.mineOnly;
  }

  public SeriesSortColumn getSortColumn() {
    return sortColumn;
  }

  public void setSortColumn(SeriesSortColumn sortColumn) {
    this.sortColumn = sortColumn;
  }

  public boolean isPendingOnly() {
    return pendingOnly;
  }

  public void setPendingOnly(boolean pendingOnly) {
    this.pendingOnly = pendingOnly;
  }

  public boolean isMineOnly() {
    return mineOnly;
  }

  public void setMineOnly(boolean mineOnly) {
    this.mineOnly = mineOnly;
  }
}
