package nl.naturalis.bcd.search;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import nl.naturalis.common.util.EnumParser;

public enum SeriesSortColumn {
  TOP_DESK_ID("a.topDeskId"),
  DEPARTMENT_NAME("e.departmentName"),
  REQUESTER_NAME("d.requesterName"),
  INSTITUTE_CODE("c.instituteCode"),
  COLLECTION_CODE("b.collectionCode"),
  FIRST_NUMBER("a.firstNumber"),
  LAST_NUMBER("a.lastNumber"),
  AMOUNT("a.amount"),
  DATE_MODIFIED("a.dateModified"),
  DISPENSE_COUNT("a.dispenseCount");

  private static final EnumParser<SeriesSortColumn> ep = new EnumParser<>(SeriesSortColumn.class);

  @JsonCreator
  public static SeriesSortColumn parse(Object value) {
    return ep.parse(value);
  }

  // The actual string to be used in the ORDER_BY clause
  private final String label;

  SeriesSortColumn(String label) {
    this.label = label;
  }

  @JsonValue
  public String toString() {
    return name();
  }

  public String getLabel() {
    return label;
  }
}
