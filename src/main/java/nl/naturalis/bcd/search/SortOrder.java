package nl.naturalis.bcd.search;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import nl.naturalis.common.util.EnumParser;

public enum SortOrder {
  ASC,
  DESC;

  private static final EnumParser<SortOrder> ep = new EnumParser<>(SortOrder.class);

  @JsonCreator
  public static SortOrder parse(Object value) {
    return ep.parse(value);
  }

  @JsonValue
  public String toString() {
    return name();
  }
}
