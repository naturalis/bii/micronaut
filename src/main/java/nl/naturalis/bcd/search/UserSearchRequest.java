package nl.naturalis.bcd.search;

public class UserSearchRequest extends SearchRequest {

  private UserSortColumn sortColumn;

  public UserSortColumn getSortColumn() {
    return sortColumn;
  }

  public void setSortColumn(UserSortColumn sortColumn) {
    this.sortColumn = sortColumn;
  }
}
