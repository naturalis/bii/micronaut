package nl.naturalis.bcd.security;

import java.util.List;

import org.pac4j.core.authorization.authorizer.AbstractCheckAuthenticationAuthorizer;
import org.pac4j.core.context.WebContext;
import org.pac4j.core.exception.http.RedirectionActionHelper;
import org.pac4j.core.profile.UserProfile;
import org.pac4j.oidc.profile.azuread.AzureAdProfile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("unused")
public class IsAdminUserAuthorizer<U extends UserProfile> extends AbstractCheckAuthenticationAuthorizer<U> {

    private static final Logger logger = LoggerFactory.getLogger(IsAdminUserAuthorizer.class);

    public IsAdminUserAuthorizer() {}

    @Override
    public boolean isAuthorized(WebContext webContext, List<U> list) {
        for (UserProfile profile : list) {
            if (profile instanceof AzureAdProfile && profile.getRoles().contains("ADMIN")) {
                logger.debug("User has role \"ADMIN\" and is allowed access");
                return true;
            }
        }
        logger.debug("User does not have role \"ADMIN\" and is not allowed access");
        return this.handleError(webContext);
    }

    @Override
    protected boolean isProfileAuthorized(WebContext webContext, U profile) {
        return (profile instanceof AzureAdProfile);
    }

    public static <U extends UserProfile> IsAdminUserAuthorizer<U> isAdminUser() {
        return new IsAdminUserAuthorizer<>();
    }

    @Override
    protected boolean handleError(WebContext context) {
        if (this.getRedirectionUrl() != null) {
            throw RedirectionActionHelper.buildRedirectUrlAction(context, this.getRedirectionUrl());
        } else {
            return false;
        }
    }
}