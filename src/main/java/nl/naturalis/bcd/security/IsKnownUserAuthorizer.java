package nl.naturalis.bcd.security;

import org.pac4j.core.authorization.authorizer.AbstractCheckAuthenticationAuthorizer;
import org.pac4j.core.context.WebContext;
import org.pac4j.core.profile.UserProfile;
import org.pac4j.oidc.profile.azuread.AzureAdProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class IsKnownUserAuthorizer<U extends UserProfile> extends AbstractCheckAuthenticationAuthorizer<U> {

    private static final Logger logger = LoggerFactory.getLogger(IsKnownUserAuthorizer.class);

    @Override
    public boolean isAuthorized(WebContext webContext, List<U> list) {
        for (UserProfile profile : list) {
            if (profile instanceof AzureAdProfile && (profile.getRoles().contains("VIEWER") || profile.getRoles().contains("USER") || profile.getRoles().contains("ADMIN"))) {
                logger.debug("User has role \"VIEWER\", \"USER\" or \"ADMIN\" and is allowed access");
                return true;
            }
        }
        return false;
    }

    @Override
    protected boolean isProfileAuthorized(WebContext webContext, U u) {
        return false;
    }

}
