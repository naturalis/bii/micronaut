SELECT COUNT(*)
  FROM bcd_collection a
 WHERE a.instituteId = IFNULL(:instituteId, -1)
   AND (
   	       ISNULL(:normalizedSearch)
   	    OR INSTR(UPPER(a.collectionCode), :normalizedSearch) != 0
   	    OR INSTR(UPPER(a.collectionName), :normalizedSearch) != 0
   	   )