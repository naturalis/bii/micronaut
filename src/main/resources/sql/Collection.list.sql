SELECT a.*,
       b.instituteCode,
       COUNT(c.requestId) as requestCount,
       IFNULL(MAX(c.lastNumber), 0) as maxNumber,
       IFNULL(SUM(c.amount), 0) as numberCount
  FROM bcd_collection a
  JOIN bcd_institute b ON a.instituteId = b.instituteId
  LEFT JOIN bcd_request c ON a.collectionId = c.collectionId
 WHERE a.instituteId = IFNULL(:instituteId, -1)
   AND (
           ISNULL(:normalizedSearch)
        OR INSTR(UPPER(a.collectionCode), :normalizedSearch) != 0
        OR INSTR(UPPER(a.collectionName), :normalizedSearch) != 0
       )
 GROUP BY a.collectionId
 ORDER BY ~%sortColumn% ~%sortOrder%
 LIMIT :offset, :limit
