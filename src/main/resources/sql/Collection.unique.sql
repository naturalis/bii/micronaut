SELECT COUNT(*)
FROM bcd_collection
WHERE (
	(ISNULL(:collectionCode) AND ISNULL(collectionCode)) OR
	UPPER(collectionCode) = UPPER(:collectionCode) OR 
	UPPER(collectionName) = UPPER(:collectionName)
)
AND instituteId = :instituteId
AND (ISNULL(:collectionId) OR collectionId != :collectionId)