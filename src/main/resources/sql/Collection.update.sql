UPDATE bcd_collection SET
	collectionName  = :collectionName
,	collectionCode  = :collectionCode
,	description     = :description
,	active      	= :active
WHERE collectionId = :collectionId