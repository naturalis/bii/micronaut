SELECT COUNT(*)
FROM bcd_department
WHERE ISNULL(:normalizedSearch) OR INSTR(UPPER(departmentName), :normalizedSearch) != 0
