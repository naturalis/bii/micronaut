SELECT a.departmentId
,	a.departmentName
,	a.active
,	COUNT(b.requestId) as requestCount
,	IFNULL(SUM(b.amount), 0) as numberCount
FROM bcd_department a
LEFT JOIN bcd_request b ON a.departmentId = b.departmentId
WHERE ISNULL(:normalizedSearch) OR INSTR(UPPER(departmentName), :normalizedSearch) != 0
GROUP BY a.departmentId
ORDER BY ~%sortColumn% ~%sortOrder%
LIMIT :offset, :limit