SELECT COUNT(*)
FROM bcd_department
WHERE UPPER(departmentName) = UPPER(:departmentName)
AND (ISNULL(:departmentId) OR departmentId != :departmentId)