SELECT cmsId AS optionValue,
       cmsName AS optionText,
       active AS 'data-active' 
  FROM bcd_cms
 WHERE active = 1 OR cmsId = :id
 ORDER BY cmsName