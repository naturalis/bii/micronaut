SELECT instituteId AS optionValue,
       instituteCode AS optionText,
       active AS 'data-active'
  FROM bcd_institute
 WHERE instituteCode NOT IN(:overlap, :corrupt)
   AND (active = 1 OR instituteId = :id)
 ORDER BY instituteCode