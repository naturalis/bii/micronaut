SELECT requesterId AS optionValue,
       requesterName AS optionText,
       active AS 'data-active'
  FROM bcd_requester
 WHERE active = 1 OR requesterId = :id
 ORDER BY requesterName