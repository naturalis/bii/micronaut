SELECT COUNT(*)
FROM bcd_requester
WHERE ISNULL(:normalizedSearch) OR INSTR(UPPER(requesterName), :normalizedSearch) != 0
