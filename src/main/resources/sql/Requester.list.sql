SELECT a.requesterId,
       a.requesterName,
       a.active,
       COUNT(b.requestId) as requestCount,
       IFNULL(SUM(b.amount), 0) as numberCount
  FROM bcd_requester a
  LEFT JOIN bcd_request b ON (a.requesterId = b.requesterId)
 WHERE ISNULL(:normalizedSearch) OR INSTR(UPPER(a.requesterName), :normalizedSearch) != 0
 GROUP BY a.requesterId
 ORDER BY ~%sortColumn% ~%sortOrder%
 LIMIT :offset, :limit