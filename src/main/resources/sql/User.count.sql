SELECT COUNT(*)
FROM bcd_user
WHERE ISNULL(:normalizedSearch) OR INSTR(UPPER(userName), :normalizedSearch) != 0
