SELECT a.*,
	COUNT(b.requestId) as requestCount,
	IFNULL(SUM(b.amount), 0) as numberCount
FROM bcd_user a
LEFT JOIN bcd_request b ON a.userId = b.modifiedBy
WHERE ISNULL(:normalizedSearch) OR INSTR(UPPER(a.userName), :normalizedSearch) != 0
GROUP BY a.userId
ORDER BY ~%sortColumn% ~%sortOrder%
LIMIT :offset, :limit