"use strict";

let minFirstNumber = -1;
let fields = null;

class FieldCache {

	instId;
	collId;
	first;
	last;
	amnt;

	constructor(jqInstituteId,jqCollectionId,jqFirstNumber,jqLastNumber,jqAmount) {
		this.instId = jqInstituteId;
		this.collId = jqCollectionId;
		this.first = jqFirstNumber;
		this.last = jqLastNumber;
		this.amnt = jqAmount;
	}

	
	// Getters for the jQuery objects themselves:
	
	instituteId() { return this.instId; };
	collectionId() { return this.collId; };
	firstNumber() { return this.first; };
	lastNumber() { return this.last; };
	amount() { return this.amnt; };
	
	
	// Getters/setters for their values:
	
	getInstituteId() { return nsParseInt(this.instId.val()); };
		
	getCollectionId() { return nsParseInt(this.collId.val()); };
	
	getFirstNumber() { return nsParseInt(this.first.val()); };	
	setFirstNumber(x) { this.first.val(x); };
	
	getLastNumber() { return nsParseInt(this.last.val()); };	
	setLastNumber(x) { this.last.val(x); };
	
	getAmount() { return nsParseInt(this.amnt.val()); };	
	setAmount(x) { this.amnt.val(x); };
	
	isNewCollection() {
		return !isNull(MODEL_BEAN.requestId)
			&& this.getCollectionId() !== null
			&& this.getCollectionId() !== MODEL_BEAN.collectionId;
	};
	
	clearNumbers() {
		this.first.val("");
		this.last.val("");
		this.amnt.val("");
	};
	
	resetNumbers() {
		this.first.val(MODEL_BEAN.firstNumber);
		this.last.val(MODEL_BEAN.lastNumber);
		this.amnt.val(MODEL_BEAN.amount);
	};
	
	updateLastNumber() {
		this.setLastNumber(this.getFirstNumber() + this.getAmount() - 1);
	}

}

/**
 * onchange handler for institutes listbox. Re-populates the
 * collections listbox.
 */
function instituteChanged() {

	// Temporarily disable everything that could spoil the operation
	fields.collectionId().off().prop("disabled", true);
	fields.firstNumber().off().prop("disabled", true);
	fields.amount().off().prop("disabled", true);
	
	// Remove current collection options
	fields.collectionId().empty();
	
	const instituteId = ifNull(fields.getInstituteId(), -1);

	// Did the user switch back to the original institute?	
	const isOriginalInstitute = instituteId === MODEL_BEAN.instituteId;
	
	let url = BASE_PATH + "institute-changed/?instituteId=" + instituteId;
	if(isNull(MODEL_BEAN.requestId)) {
		// requestId is primary key, so if null, we are creating a new
		// number series instead of updating an existing one.
		url += "&collectionId=-1";
	} else if(isOriginalInstitute) {
		// The user has re-selected the original institute. Let's give
		// him/er back the original collection as well. By providing the
		// original collectionId we ensure that it will be among the
		// returned options, even if the collection has meanwhile
		// become inactive.
		url += "&collectionId=" + MODEL_BEAN.collectionId;
	} else {
		url += "&collectionId=-1";
	}
	$.ajax({
	    type: "GET",
	    url: url,
	    dataType: "html",
	    success: function(data) {
	    	fields.collectionId().html(data);
	    	collectionChanged();
	    }
	});	
		
	// Re-enable handlers
	fields.collectionId().prop("disabled", false).change(collectionChanged);
	fields.firstNumber()
		.prop("disabled", false)
		.keypress(function(event) { return isPositiveInteger($(this), event);})
		.change(firstNumberChanged);
	fields.amount()
		.prop("disabled", false)
		.keypress(function(event) { return isPositiveInteger($(this), event);})
		.change(amountChanged);
	
}

/**
 * onchange handler for the collections listbox. Retrieves the next available
 * number for the selected collection. That is: the absolute highest lastNumber
 * plus one for that collection.
 */
function collectionChanged() {
	if(fields.getCollectionId() === null) {
		fields.clearNumbers();
	} else if(fields.getCollectionId() === MODEL_BEAN.collectionId) {
		fields.resetNumbers();
		getMinFirstNumberForSeries();
	} else {
		getMinFirstNumberForCollection();
	}
}

function showOrHideInactiveWarning() {
	const e = $(this);
	const active = e.find("option:selected").attr("data-active");
	if(active !== undefined) {
		if(parseInt (active) === 1) {
			e.parent().find("span").css("visibility", "hidden");
		} else {
			e.parent().find("span").css("visibility", "visible");
		}
	}
}

/**
 * keyup handler for firstNumber and amount fields.
 */
function numberEdited() {
	if(fields.getFirstNumber() === null || fields.getAmount() === null) {
		fields.setLastNumber("");
	} else {
		fields.updateLastNumber();
	}
}

/**
 * change handler for firstNumber input control.
 */
function firstNumberChanged() {
	if(fields.getFirstNumber() === null) {
		fields.setLastNumber("");
	} else if(isNull(MODEL_BEAN.requestId)) {
		if(minFirstNumber === -1) {
			return; // User has not selected a collection yet
		} else if(fields.getFirstNumber() < minFirstNumber) {
			showError("Eerste nummer moet minimaal " + minFirstNumber + " zijn voor deze collectie");
			fields.setFirstNumber(minFirstNumber);
		} else if(fields.getAmount() !== null) {
			fields.updateLastNumber();
		}
	} else if(fields.getFirstNumber() < minFirstNumber) {
		if(fields.isNewCollection()) {
			showError("Eerste nummer moet minimaal " + minFirstNumber + " zijn voor deze collectie");
			fields.setFirstNumber(minFirstNumber);
		} else {
			showError("Eerste nummer moet minimaal " + minFirstNumber + " zijn");
			fields.setFirstNumber(MODEL_BEAN.firstNumber);
		}
	} else if(fields.getAmount() !== null) {
		fields.updateLastNumber();
	}
}

function amountChanged() {
	if(fields.getAmount() === null) {
		fields.setLastNumber("");
	} else if(fields.getFirstNumber() !== null) {
		fields.updateLastNumber();
	}
}

function getMinFirstNumberForCollection() {
	$.ajax({
	    type: "GET",
	    url: BASE_PATH + "min-first-number-for-collection/" + fields.getCollectionId(),
	    dataType: "json",
	    success: function(data) {
	    	minFirstNumber = parseInt(data);
	    	console.log("Minimum allowed first number for current collection: " + minFirstNumber);
	    	fields.setFirstNumber(minFirstNumber);
	    	if(fields.getAmount() !== null) {
	    		fields.updateLastNumber();
	    	}
	    }
	});
}

// Get the smallest number the user is allowed to enter into the firstNumber field.
// The user may enter a smaller number than the current first number of the series,
// but only if this was the most recently added number series, in which case he/she
// may enter anything down to the last number (plus 1) of the previous series.
function getMinFirstNumberForSeries() {
	$.ajax({
	    type: "GET",
	    url: BASE_PATH + "min-first-number-for-series/" + fields.getCollectionId() + "/" + MODEL_BEAN.firstNumber,
	    dataType: "json",
	    success: function(data) {
	    	minFirstNumber = parseInt(data);
	    	console.log("Minimum allowed first number for current series: " + minFirstNumber);
	    }
	});
}

/**
 * Save button click handler.
 */
function prepareSaveOrUpdate() {
	const emptyCount = checkRequiredFields();
	if(emptyCount !== 0) {
		showError("Niet alle verplichte velden zijn ingevuld");
	} else {
		const changedCount = updateModel();
		if(changedCount === 0) {
			showWarning("Formulier nog niet ingevuld / gewijzigd");
		} else {
			// These error conditions should not be possible, but
			// let's check them anyhow
			if(fields.getFirstNumber() < 1) {
				showError("Eerst nummer moet groter dan 0 zijn");
			} else if(fields.getAmount() < 1) {
				showError("Aantal moet groter dan 0 zijn");
			} else if(fields.getLastNumber() < fields.getFirstNumber()) {
				showError("Laatste nummer mag niet kleiner zijn dan eerste nummer");
			} else if((fields.getFirstNumber() + fields.getAmount() !== fields.getLastNumber() + 1)) {
				showError("Optelsom klopt niet");
			} else {
				checkOverlapAndSave();
			}
		}
	}
}

/**
 * Asks the server whether the current form input would consititute a valid
 * number series. If so, goes on to post the form data.
 */
function checkOverlapAndSave() {
	$.ajax({
	    type: "POST",
	    url: BASE_PATH + "find-overlap",
	    data: JSON.stringify(MODEL_BEAN),
	    contentType: "application/json; charset=utf-8",
	    dataType: "json",
	    success: function(data) {
			if(data.length !== 0) {
				showError(
					"Deze nummerreeks overlapt met de volgende " +
					"nummerreeksen: " + data.join(", ")
				);
			} else {
				saveOrUpdate("Nummerreeks");
			}
	    }
	});
}

function lockNumberSeries(id) {
	if(isDirty()) {
		showError("Wijzigingen moeten eerst worden opgeslagen");
	} else {
		action(id + "/lock");
	}
}
