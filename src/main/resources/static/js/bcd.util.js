"use strict";

/**************************************************/
/************** JAVASCRIPT UTILITIES **************/
/**************************************************/

function isCheckBox(jquery) {
	return jquery.is("input[type=checkbox]");
}

function isSelectRoleElement(jquery) {
	return jquery.is("select[name=role]");
}

function isRadio(jquery) {
	return jquery.is("input[type=radio]");
}

function isFileInput(jquery) {
	return jquery.is("input[type=file]");
}

function getRadioValue(name) {
	const e = $("input[name='" + name + "']:checked");
	if(e.length === 0 || !isRadio(e)) {
		console.log("No such radio button: <input type=\"radio\" name=\"" + name + "\">");
		return undefined;
	}
	return e.val();
}

function isSelectedRadioValue(name, value) {
	const e = $("input[name='" + name + "']:checked");
	if(!isRadio(e)) {
		console.log("No such radio button: <input type=\"radio\" name=\"" + name + "\">");
		return undefined;
	}
	return e.val() === value;
}

function setRadioValue(name, value) {
	const e = $("input[name='" + name + "']");
	if(e.length === 0 || !isRadio(e)) {
		console.log("No such radio button: <input type=\"radio\" name=\"" + name + "\">");
		return undefined;
	}
	e.each(function() {
		const button = $(this);
		if(button.val() === value) {
			button.prop("checked", true);
		}
	});
}

function checkRadioButton(id) {
	const e = $("#" + id);
	if(e.length === 0 || !isRadio(e)) {
		console.log("No such radio button: <input type=\"radio\" id=\"" + id + "\">");
		return;
	}
	e.prop("checked", true);
}

function isCheckedRadioButton(id) {
	const e = $("#" + id);
	if(e.length === 0 || !isRadio(e)) {
		console.log("No such radio button: <input type=\"radio\" id=\"" + id + "\">");
		return undefined;
	}
	return e.prop("checked");
}

function goto(path) {
	window.location.href = path;
}

// Makes us invulnerable to how null is serialized on the server.
function isNull(x) {
	return x === null || x === undefined;
}

function isEmpty(x) {
	if(typeof x === "string") {
		return x.trim().length === 0;
	} else if(isNull(x)) {
		return true;
	} else if(Array.isArray(x)) {
		return x.length === 0;
	} else if(typeof x.jquery === "string" && typeof x.val === "function") {
		return isEmpty(x.val());
	}
	return isEmpty(x.toString());
}

// Empty-to-null. This function will return either null, or a non-blank string
// or a non-empty array. Numbers will be converted to strings. You can also
// pass jQuery objects, in which case their val() function is called if defined.
// For non-blank strings any CR LF sequence is replaced with just LF. This is
// to ensure consistent GET and SET behaviour for text areas.
function e2n(x) {
	if(typeof x === "string") {
		return (x = x.trim()).length === 0? null : x.replace(/\r\n/g, "\n");
	} else if(isNull(x)) {
		return null;
	} else if(typeof x.jquery === "string" && typeof x.val === "function") {
		return e2n(x.val());
	} else if(Array.isArray(x)) {
		return  x.length === 0? null: x;
	}
	return e2n(x.toString());
}

function ifNull(x,y) {
	return isNull(x)? y : x;
}

// Null-safe parseInt. NB This function behaves well if the argument is
// null or undefined, but it _does_ return null in either of those cases
// (unless a default value is specified).
function nsParseInt(str,dfault) {
	// Also allow the argument to be an integer already, so we can more
	// or less blindly use this function
	if(typeof str === 'number') {
		if(Math.round(str) !== str) {
			throw "Not an integer: " + str;
		}
		return str;
	}
	if(isNull(str) || str === "") {
		if(typeof dfault !== 'undefined') {
			return dfault;
		}
		return null;
	}
	if(typeof str !== 'string') {
		throw "Illegal data type for argument \"str\": " + (typeof str);
	}
	const i = parseInt(str);
	if(isNaN(i) || i.toString() !== str) {
		throw "Not an integer: " + str;
	}
	return i;
}

// Centers an element, or recenters it when used as onresize handler.
function recenter(jquery) {
	const left = ($(document).width() / 2) - (jquery.width() / 2);
	jquery.css("left", left + "px");
}

/************** KEY LISTENER FUNCTIONALITY **************/

const  KEY_BACKSPACE = 8;
const  KEY_SHIFT = 16;
const  KEY_CTRL = 17;
const  KEY_ALT = 18;
const  KEY_CAPSLOCK = 20;
const  KEY_PAGE_UP = 33;
const  KEY_PAGE_DOWN = 34;
const  KEY_END = 35;
const  KEY_HOME = 36;
const  KEY_ARROW_LEFT = 37;
const  KEY_ARROW_UP = 38;
const  KEY_ARROW_RIGHT = 39;
const  KEY_ARROW_DOWN = 40;
const  KEY_INSERT = 45;
const  KEY_DELETE = 46;

const  KEY_TAB = 9;
const  KEY_ENTER = 13;
const  KEY_SPACE = 32;

const  EDIT_KEYS = [
	KEY_BACKSPACE,
	KEY_SHIFT,
	KEY_CTRL,
	KEY_ALT,
	KEY_CAPSLOCK,
	KEY_PAGE_UP,
	KEY_PAGE_DOWN,
	KEY_END,
	KEY_HOME,
	KEY_ARROW_LEFT,
	KEY_ARROW_UP,
	KEY_ARROW_RIGHT,
	KEY_ARROW_DOWN,
	KEY_INSERT,
	KEY_DELETE
];

function isEditKey(code) {
	return EDIT_KEYS.includes(code);
}

function isDigitKey(code) {
	return code >= 48 && code <= 57;
}

// Keypress handler for form fields that must be whole numbers (digits only).
function isPositiveInteger(input, event) {
	// 1st arg is jQuery wrapper around form field
	if(parseInt(input.val()) > 2147483647) {
		return false;
	}
	const i = event.which;
	if(input.val() === "" && i === 48) {
		return false;
	}
	if(isDigitKey(i) || isEditKey(i)) {
		return true;
	}
	if(i === KEY_TAB || i === KEY_ENTER) {
		return true; // allow user to navigate out of the field
	}
	return false;
}
