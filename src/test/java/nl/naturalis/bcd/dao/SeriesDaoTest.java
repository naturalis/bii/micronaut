package nl.naturalis.bcd.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Optional;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import nl.naturalis.bcd.exception.BcdConfigurationException;
import nl.naturalis.bcd.json.BcdModule;
import nl.naturalis.bcd.managed.Database;
import nl.naturalis.bcd.model.NumberSeries;
import static org.junit.jupiter.api.Assertions.assertFalse;

@Disabled
public class SeriesDaoTest {

  @BeforeAll
  public static void init() throws Exception {
    new Database(new TestDbConfig()).start();
  }

  @AfterAll
  public static void done() throws Exception {
    Database.instance().stop();
  }

  @Test
  public void find00() {
    SeriesDao dao = new SeriesDao();
    Optional<NumberSeries> opt = dao.find(NumberSeries.class, NumberSeries::new, 1119);
    NumberSeries ns = opt.get();
    assertFalse(opt.isEmpty());
    System.out.println(BcdModule.writeJson(ns));
  }

  @Test
  public void find01() throws BcdConfigurationException, SQLException {
    String sql =
        "INSERT INTO Test (timsestamp0, date0, bigint0, varchar0, boolean0) VALUES(?,?,?,?,?)";
    try (Connection con = Database.connect()) {
      try (PreparedStatement ps = con.prepareStatement(sql)) {
        ps.setObject(1, LocalDateTime.now());
        ps.setString(2, "1980-03-25");
        ps.setString(3, "777");
        ps.setInt(4, 8);
        ps.setBoolean(5, false);
        ps.executeUpdate();
      }
    }
  }
}
