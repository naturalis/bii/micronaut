package nl.naturalis.bcd.dao;

import nl.naturalis.bcd.DbConfig;

public class TestDbConfig extends DbConfig {

  public TestDbConfig() {
    host = "localhost";
    port = "3306";
    user = "admin";
    password = "admin";
    name = "BCD";
  }
}
