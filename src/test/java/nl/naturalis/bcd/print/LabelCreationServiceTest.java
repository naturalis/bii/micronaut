package nl.naturalis.bcd.print;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import nl.naturalis.bcd.dao.TestDbConfig;
import nl.naturalis.bcd.managed.Database;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Disabled
public class LabelCreationServiceTest {

  @BeforeAll
  public static void init() throws Exception {
    new Database(new TestDbConfig()).start();
  }

  @AfterAll
  public static void done() throws Exception {
    Database.instance().stop();
  }

  /*
   * As long as the numbers in a number series are integers, they cannot possibly be more than 10
   * characters wide, so the test in the original LabelCreationService is redundant.
   */
  @Test
  public void test00() {
    assertTrue((int) Math.log10(Integer.MAX_VALUE) <= 10);
    System.out.println((int) Math.log10(11));
  }
}
