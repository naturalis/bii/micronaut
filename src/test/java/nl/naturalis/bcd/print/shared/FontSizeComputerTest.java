package nl.naturalis.bcd.print.shared;

import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.junit.jupiter.api.Test;
import nl.naturalis.bcd.model.Layout;
import nl.naturalis.bcd.model.NumberSeries;
import nl.naturalis.bcd.print.NumberSeriesPrintSession;
import nl.naturalis.bcd.print.LabelWriter;
import nl.naturalis.bcd.print.LayoutData;
import nl.naturalis.bcd.print.LayoutDataFactory;
import nl.naturalis.bcd.print.std.StdLayoutData;

@SuppressWarnings("unused")
public class FontSizeComputerTest {

  @Test
  public void test00() {
    StdLayoutData ld = new StdLayoutData();
    ld.setImgSize(10);
    ld.setTextWidth(15);
    NumberSeries ns = new NumberSeries();
  }
}
